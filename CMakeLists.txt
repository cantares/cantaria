cmake_minimum_required(VERSION 3.8)
project(cantaria)

set(CMAKE_CXX_STANDARD 11)

string(TOLOWER  ${CMAKE_SYSTEM_NAME} CTR_OS)
if(NOT CTR_ARCH)
    if(CMAKE_SIZEOF_VOID_P EQUAL 8)
        set(CTR_ARCH "x86_64")
    else()
        set(CTR_ARCH "x86")
    endif()
endif()

message(STATUS "CMAKE_GENERATOR: ${CTR_OS} ${CTR_ARCH}")

#add_compile_definitions(DEBUG)

include_directories(libs/log)
include_directories(libs/bass/include)
include_directories(libs/midifile/include)
include_directories(libs/filesystem/include)
include_directories(libs/rapidjson/include)
include_directories(libs/sqlite/src)
#include_directories(libs/jdksmidi/include)

add_subdirectory(libs/midifile)
add_subdirectory(libs/filesystem)
#add_subdirectory(libs/rapidjson)
add_subdirectory(libs/sqlite)

set_property(TARGET midifile PROPERTY POSITION_INDEPENDENT_CODE ON)

list(APPEND IMPORTED_LIB_DIRS ${PROJECT_SOURCE_DIR}/libs/bass/bin/${CTR_OS}/${CTR_ARCH})

if(WIN32)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mwindows")
    set(CMAKE_SHARED_LIBRARY_PREFIX "")
    set(CMAKE_STATIC_LIBRARY_PREFIX "")
    set(CMAKE_OBJDUMP "objdump")

    include_directories(libs/aubio/include)

    list(APPEND IMPORTED_LIB_DIRS ${PROJECT_SOURCE_DIR}/libs/aubio/bin/${CTR_OS}/${CTR_ARCH})
    add_library(aubio SHARED IMPORTED GLOBAL)
    set_property(TARGET aubio PROPERTY IMPORTED_LOCATION "${PROJECT_SOURCE_DIR}/libs/aubio/bin/${CTR_OS}/${CTR_ARCH}/libaubio-5${CMAKE_SHARED_LIBRARY_SUFFIX}")
    set_property(TARGET aubio PROPERTY IMPORTED_IMPLIB "${PROJECT_SOURCE_DIR}/libs/aubio/bin/${CTR_OS}/${CTR_ARCH}/libaubio-5${CMAKE_SHARED_LIBRARY_SUFFIX}" )
endif()

link_directories(${PROJECT_SOURCE_DIR}/libs/bass/bin/${CTR_OS}/${CTR_ARCH})
find_library(bass NAMES bass PATHS ${PROJECT_SOURCE_DIR}/libs/bass/bin/${CTR_OS}/${CTR_ARCH} REQUIRED)
find_library(bass_fx NAMES bass_fx PATHS ${PROJECT_SOURCE_DIR}/libs/bass/bin/${CTR_OS}/${CTR_ARCH} REQUIRED)
find_library(bassmidi NAMES bassmidi PATHS ${PROJECT_SOURCE_DIR}/libs/bass/bin/${CTR_OS}/${CTR_ARCH} REQUIRED)

message(STATUS "BASS: ${bass}")

#file(GLOB jdksmidi_internals "libs/jdksmidi/include/jdksmidi/*.h" "libs/jdksmidi/src/*.cpp")
#add_library(jdksmidi SHARED ${jdksmidi_internals} libs/jdksmidi/jdksmidi.cpp libs/jdksmidi/jdksmidi.h libs/jdksmidi/jdksmidi_global.h)

add_subdirectory(core)
add_subdirectory(qtapp)