git submodule update --init --recursive
mkdir cmake-build
cd cmake-build
cmake -DCMAKE_BUILD_TYPE=Release -G "Unix Makefiles" ..
cmake --build qtapp