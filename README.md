# Introduction

Cantaria is a free, open source music player, with a pitch detection and display feature similar to that of SingStar like games.
Our goal is to create an easy to use and versatile player for practicing singing skills.

# Releases

The latest release package is available for download at https://cantaria.app/ or at https://gitlab.com/cantares/cantaria/-/releases

# Installation

### Windows

The windows release package is a self contained installer. After running it, the application should work out of the box.

### Linux

Unzip the release package in the directory of your choice. Make sure the following packages are installed on your machine:

```
sudo apt install libqt5core5a libstdc++6 libx11-6 libqt5widgets5 libqt5network5 libqt5gui5 libqt5xml5 libqt5x11extras5 aubio-tools
```

Afterwards the executable should run without issues.

# Building

### Linux

The following packages need to be installed to build Cantaria:

```
sudo apt install build-essential cmake qt5-default aubio-tools libaubio-dev libaubio-doc libasound-dev 
```