#include "mainwindow.h"
#include "ui_mainwindow.h"

namespace Cantaria
{
	namespace QtGui
	{

		MainWindow::MainWindow(QWidget *parent) :
			QMainWindow(parent, Qt::FramelessWindowHint | Qt::WindowSystemMenuHint),
			ui(new Ui::MainWindow),
			cantariaContext(new CantariaContext()),
			lowestFPS(100),
			detachViewTime(false),
			showTutorial(false),
			skinMgr(nullptr),
			networkManager(nullptr)
		{
			setFocusPolicy(Qt::StrongFocus);
			Output2FILE::Stream() = fopen("cantaria.log", "w");
			QCoreApplication::setApplicationName("Cantaria");
			ui->setupUi(this);
			ui->windowBar->showMinimize();

			QString location = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + "/cantaria/";

			cantariaContext->SingDevice = createCantariaDevice(location.toStdString());
			cantariaContext->AudioAnalyzer = cantariaContext->SingDevice->getAudioAnalyzer();
			cantariaContext->AudioInput = cantariaContext->SingDevice->getAudioInput();
			cantariaContext->AudioAnalyzer->setVolumeThreshold(0.05f);
			cantariaContext->AudioAnalyzer->setSamplesArraySize(1024);
			cantariaContext->SongControl = cantariaContext->SingDevice->getSongControl();
			cantariaContext->SongDatabase = cantariaContext->SingDevice->getSongDatabase();
			settings = cantariaContext->SongControl->getSettings();
			settings->setSelectedTrackIndex(0);
			settings->setOutputLatency(150);
			settings->setViewedTime(0);

			outputSettings.init(cantariaContext);
			playlist.init(cantariaContext);
			songWindow.init(cantariaContext);
			settingsWindow.init(cantariaContext);
			songSearch.init(cantariaContext);

			cantariaContext->SongControl->addSongLoadedCallback(MainWindow::updateSong, this);
			connect(&settingsWindow, SIGNAL(skinSet(const SkinManager*)), this, SLOT (setSkin(const SkinManager*)));
			connect(&settingsWindow, SIGNAL(skinSet(const SkinManager*)), &songSearch, SLOT (skinChanged(const SkinManager*)));
			connect(&settingsWindow, SIGNAL(textLinesSet(int)), &songWindow, SLOT (setTextLines(int)));
			connect(ui->windowBar, SIGNAL(exitPushed()), this, SLOT(close()));
			connect(&tutorial, SIGNAL(pageChanged(QString, int)), this, SLOT(tutorialPageChange(QString, int)));
			connect(&songSearch, SIGNAL(searchResultPicked(const QString&)), &playlist, SLOT (addFile(const QString&)));

			outputSettings.installEventFilter(this);
			playlist.installEventFilter(this);
			songWindow.installEventFilter(this);
			settingsWindow.installEventFilter(this);
			tutorial.installEventFilter(this);
			songSearch.installEventFilter(this);
			installEventFilter(&playlist);

			ui->songCheckbox->installEventFilter(this);

			networkManager = new QNetworkAccessManager(this);
			connect(networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(on_checkUpdatesResponse(QNetworkReply*)));

			move(50, 20);
			playlist.move(x(), frameGeometry().bottom()+1);
			outputSettings.move(frameGeometry().right()+1, frameGeometry().top());
			songSearch.move(outputSettings.frameGeometry().right()+1, frameGeometry().top());
			settingsWindow.move(songSearch.frameGeometry().right()+1, frameGeometry().top());
			songWindow.move(frameGeometry().right()+1, frameGeometry().bottom()+1);

			setAcceptDrops(true);
			settingsWindow.setCurrentSkin("");

			playlist.loadPlaylist(QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + "/cantaria/playlist." + LIST_EXT);
			loadSettings();
			outputSettings.updateSettings();

			settingsWindow.refreshSettings();

			checkUpdates();
			timer.start(15, this);
		}

		MainWindow::~MainWindow()
		{
#ifdef DEBUG
			FILE_LOG(logDEBUG) << "[MainWindow]: Lowest FPS: " << lowestFPS;
#endif
			removeEventFilter(&playlist);
			outputSettings.removeEventFilter(this);
			playlist.removeEventFilter(this);
			songWindow.removeEventFilter(this);
			settingsWindow.removeEventFilter(this);
			tutorial.removeEventFilter(this);
			songSearch.removeEventFilter(this);
			tutorial.close();

			if(networkManager) {
				delete networkManager;
				networkManager = nullptr;
			}

			cantariaContext->SingDevice->release();
			delete cantariaContext;
			delete ui;
		}

		void MainWindow::show()
		{
			QMainWindow::show();
			outputSettings.show();
			playlist.show();
			songWindow.show();
			settingsWindow.show();
			songSearch.show();
			if(showTutorial)
			{
				tutorial.setDocumentName("tutorial");
				tutorial.setPage(0);
				tutorial.show();
			}
			#ifndef DEBUG
            ui->label->hide();
			#endif
		}

		void MainWindow::loadSettings()
		{
			QString location = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
			QFile file(location + "/cantaria/settings.xml");

			if(file.open(QIODevice::ReadOnly))
			{
				QDomDocument doc("settings");
				doc.setContent(&file);
				file.close();

				QDomElement docElement = doc.documentElement();

				QDomNode node = docElement.firstChildElement("InputDevice");
				if(!node.isNull())
				{
					cantariaContext->AudioInput->setDevice(node.attributes().namedItem("Id").nodeValue().toInt());
				}
				node = docElement.firstChildElement("InputSamples");
				if(!node.isNull())
				{
					cantariaContext->AudioAnalyzer->setSamplesArraySize(node.attributes().namedItem("Samples").nodeValue().toInt());
				}
				node = docElement.firstChildElement("Volume");
				if(!node.isNull())
				{
					cantariaContext->SongControl->getSettings()->setVolume(
								node.attributes().namedItem("Volume").nodeValue().toInt());
				}
				node = docElement.firstChildElement("VolumeTreshold");
				if(!node.isNull())
				{
					cantariaContext->AudioAnalyzer->setVolumeThreshold(
								node.attributes().namedItem("Treshold").nodeValue().toFloat());
				}
				node = docElement.firstChildElement("TextGroupBy");
				if(!node.isNull())
				{
					settings->setTextGroupBy(
								node.attributes().namedItem("GroupBy").nodeValue().toInt());
				}
				node = docElement.firstChildElement("TextLineCount");
				if(!node.isNull())
				{
					settings->setTextLineCount(
								node.attributes().namedItem("LineCount").nodeValue().toInt());
				}
				node = docElement.firstChildElement("TimeShift");
				if(!node.isNull())
				{
					settings->setOutputLatency(
								node.attributes().namedItem("Shift").nodeValue().toInt());
				}
				node = docElement.firstChildElement("TimeRange");
				if(!node.isNull())
				{
					settings->setViewTimeRange(
								node.attributes().namedItem("Range").nodeValue().toInt());
				}
                node = docElement.firstChildElement("Soundfont");
                if(!node.isNull())
                {
                    QFileInfo fInfo;
                    fInfo.setFile(node.attributes().namedItem("Filename").nodeValue());
                    outputSettings.lastOpenedDir = fInfo.absoluteDir();
                    songSearch.lastOpenedDir = fInfo.absoluteDir();

                    settings->setSoundfont(fInfo.absoluteFilePath().toStdString());
                }
				node = docElement.firstChildElement("LastOpenedDir");
				if(!node.isNull())
				{
					playlist.lastOpenedDir = QDir(node.attributes().namedItem("Path").nodeValue());
				}
                node = docElement.firstChildElement("MinimumAccuracy");
                if(!node.isNull())
                {
                    settings->setExcMinimumAccuracy(
                            node.attributes().namedItem("Accuracy").nodeValue().toFloat());
                }

				QDomElement guiSection = docElement.firstChildElement("gui");
				loadWinProperties(this, &guiSection);
				loadWinProperties(&settingsWindow, &guiSection);
				loadWinProperties(&outputSettings, &guiSection);
				loadWinProperties(&playlist, &guiSection);
				loadWinProperties(&songWindow, &guiSection);
				loadWinProperties(&songSearch, &guiSection);

				node = guiSection.firstChildElement(songSearch.objectName());
				if(!node.isNull())
				{
					songSearch.setColumnWidhts(node.attributes().namedItem("columns").nodeValue());
				}

				node = guiSection.firstChildElement("Skin");
				settingsWindow.setCurrentSkin(node.isNull() ? "" : node.attributes().namedItem("Name").nodeValue());
			}
			else
			{
				//if there was no settings file, then assume this is the first run and open the tutorial
				showTutorial = true;
				playlist.clearPlaylist();
				playlist.addFile("samples/jingle_bells.kar");
				playlist.loadNextSong();
			}
		}

		void MainWindow::saveSettings()
		{
			QString location = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation) + "/cantaria";
			if(!QDir(location).exists())
				QDir().mkdir(location);

			playlist.savePlaylist(location + "/playlist." + LIST_EXT);

			QFile file(location + "/settings.xml");

			QDomDocument doc("settings");
			if(file.open(QIODevice::ReadOnly))
			{
				doc.setContent(&file);
				file.close();
			}

			QDomElement root = getNode("settings", &doc, &doc);
			QDomElement node = getNode("InputDevice", &doc, &root);
			node.setAttribute("Id", cantariaContext->AudioInput->getCurrentDevice().Id);

			node = getNode("InputSamples", &doc, &root);
			node.setAttribute("Samples", cantariaContext->AudioAnalyzer->getSamplesArraySize());

			node = getNode("Volume", &doc, &root);
			node.setAttribute("Volume", cantariaContext->SongControl->getSettings()->getVolume());

			node = getNode("VolumeTreshold", &doc, &root);
			node.setAttribute("Treshold", cantariaContext->AudioAnalyzer->getVolumeThreshold());

			node = getNode("TextGroupBy", &doc, &root);
			node.setAttribute("GroupBy", settings->getTextGroupBy());

			node = getNode("TextLineCount", &doc, &root);
			node.setAttribute("LineCount", settings->getTextLineCount());

			node = getNode("TimeShift", &doc, &root);
			node.setAttribute("Shift", (qlonglong)settings->getOutputLatency());

			node = getNode("TimeRange", &doc, &root);
			node.setAttribute("Range", (qlonglong)settings->getViewTimeRange());

            node = getNode("Soundfont", &doc, &root);
            node.setAttribute("Filename", QString::fromStdString(settings->getSoundfont()));

            node = getNode("MinimumAccuracy", &doc, &root);
            node.setAttribute("Accuracy", settings->getExcMinimumAccuracy());

			node = getNode("LastOpenedDir", &doc, &root);
			node.setAttribute("Path", playlist.lastOpenedDir.absolutePath());

			QDomElement guiNode = getNode("gui", &doc, &root);
			saveWinProperties(this, &doc, &guiNode);
			saveWinProperties(&settingsWindow, &doc, &guiNode);
			saveWinProperties(&outputSettings, &doc, &guiNode);
			saveWinProperties(&playlist, &doc, &guiNode);
			saveWinProperties(&songWindow, &doc, &guiNode);
			saveWinProperties(&songSearch, &doc, &guiNode);

			node = getNode(songSearch.objectName(), &doc, &guiNode);
			node.setAttribute("columns", songSearch.getColumnWidths());

			node = getNode("Skin", &doc, &guiNode);
			QString v = skinMgr ? skinMgr->getSkinName() : "";
			node.setAttribute("Name", v);

			if(file.open(QIODevice::WriteOnly))
			{
				QTextStream stream(&file);
				stream << doc.toString();
				file.close();
			}
		}

		void MainWindow::saveWinProperties(QWidget *window, QDomDocument *doc, QDomNode *parent)
		{
			QDomElement node = getNode(window->objectName(), doc, parent);
			node.setAttribute("x", window->geometry().x());
			node.setAttribute("y", window->geometry().y());
			node.setAttribute("w", window->geometry().width());
			node.setAttribute("h", window->geometry().height());
			node.setAttribute("visible", window->isVisible());
		}

		void MainWindow::loadWinProperties(QWidget *window, QDomElement *docElement)
		{
			QDomElement node = docElement->firstChildElement(window->objectName());
			if(!node.isNull())
			{
				window->setGeometry(node.attributes().namedItem("x").nodeValue().toInt(),
									node.attributes().namedItem("y").nodeValue().toInt(),
									node.attributes().namedItem("w").nodeValue().toInt(),
									node.attributes().namedItem("h").nodeValue().toInt());

				if(window != this)
					((SubWindow *)window)->setAllowShow(node.attributes().namedItem("visible").nodeValue().toInt());
			}
		}

		QDomElement MainWindow::getNode(QString name, QDomDocument *doc, QDomNode *parent)
		{
			QDomElement node = parent->firstChildElement(name);
			if(node.isNull())
			{
				node = doc->createElement(name);
				parent->appendChild(node);
			}
			return node;
		}

		void MainWindow::checkUpdates()
		{
			networkManager->get(QNetworkRequest(QUrl("https://cantaria.app/api/v1/releases")));
		}
	}
}
