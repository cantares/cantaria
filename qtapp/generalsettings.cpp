#include "settings.h"
#include "ui_settings.h"

namespace Cantaria
{
	namespace QtGui
	{

		Settings::Settings(QWidget *parent) : SubWindow(parent),
			ui(new Ui::Settings)
		{
			ui->setupUi(this);
			setSubWidgets(ui->windowBar, ui->content);
		}

		Settings::~Settings()
		{
			delete ui;
		}

		void Settings::show()
		{
			refresh();
			SubWindow::show();
		}

		void Settings::refresh()
		{
			ui->skinsList->clear();
			QDir dir("skins");
			if(dir.exists())
			{
				ui->skinsList->addItems(dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot));
			}

			for(int i = 0; i < ui->skinsList->count(); i++)
			{
				if(ui->skinsList->item(i)->text() == skinManager.getSkinName())
				{
					ui->skinsList->setHighlightedItem(i);
					break;
				}
			}

			ui->latencySlider->setValue(context->SongControl->getSettings()->getOutputLatency() / 10);
			ui->timeRangeSlider->setValue(context->SongControl->getSettings()->getViewTimeRange() / 100);
			ui->textLinesBox->setValue(context->SongControl->getSettings()->getTextLineCount());
			ui->groupedByBox->setValue(context->SongControl->getSettings()->getTextGroupBy());
            ui->toleranceSlider->setValue(context->SongControl->getSettings()->getExcMinimumAccuracy() * 100);
		}

		void Settings::setCurrentSkin(QString name)
		{
			skinManager.setSkin(name);
			emit skinSet(&skinManager);
		}

		void Settings::on_latencySlider_valueChanged(int value)
		{
			context->SongControl->getSettings()->setOutputLatency(value*10);
			ui->latencyValueLabel->setText(QString("%1 ms").arg(value*10));
		}

		void Settings::on_timeRangeSlider_valueChanged(int value)
		{
			context->SongControl->getSettings()->setViewTimeRange(value*100);
			ui->timeRangeValueLabel->setText(QString("%1 ms").arg(value*100));
		}

		void Settings::on_skinsList_itemDoubleClicked(QListWidgetItem *item)
		{
			ui->skinsList->setHighlightedItem(item->listWidget()->currentRow());
			setCurrentSkin(item->text());
		}

		void Settings::on_textLinesBox_valueChanged(int arg1)
		{
			context->SongControl->getSettings()->setTextLineCount(arg1);
			ui->groupedByBox->setMaximum(arg1);
			emit textLinesSet(arg1);
		}

		void Settings::on_groupedByBox_valueChanged(int arg1)
		{
			context->SongControl->getSettings()->setTextGroupBy(arg1);
		}

        void Settings::on_loadAsExcersizeCheckbox_clicked(bool checked)
        {
            context->SongControl->getSettings()->setExcMode(checked);

		    if (checked) {
                context->SongControl->getSettings()->setExcRounds(ui->roundsSlider->value());
                context->SongControl->getSettings()->setExcRoundIncrement(ui->roundIncSlider->value());
                context->SongControl->getSettings()->setExcReturnToStart(ui->reverseCheckbox->isChecked());
                context->SongControl->getSettings()->setExcStartOffset(ui->offsetSlider->value());
		    } else {
                context->SongControl->load(
                        context->SongControl->getSong()->getProperty(CTR_SONG_PROP_ABSOLUTEPATH),
						true
                );
            }
        }

        void Settings::on_roundIncSlider_valueChanged(int arg1)
        {
            context->SongControl->getSettings()->setExcRoundIncrement(arg1);
            ui->roundIncValueLabel->setText(QString("%1 notes").arg(arg1));
        }

        void Settings::on_toleranceSlider_valueChanged(int arg1)
        {
            context->SongControl->getSettings()->setExcMinimumAccuracy(arg1 / 100.0f);
            ui->toleranceLevelLabel->setText(QString("%1%").arg(arg1));
        }

        void Settings::on_offsetSlider_valueChanged(int arg1)
        {
            context->SongControl->getSettings()->setExcStartOffset(arg1);
            ui->offsetValueLabel->setText(QString("%1 notes").arg(arg1));
        }

        void Settings::on_roundsSlider_valueChanged(int arg1)
        {
            context->SongControl->getSettings()->setExcRounds(arg1);
            ui->roundsValueLabel->setText(QString("%1 rounds").arg(arg1));
        }

        void Settings::on_reverseCheckbox_clicked(bool checked)
        {
            context->SongControl->getSettings()->setExcReturnToStart(checked);
        }

        void Settings::updateSettings()
        {
		    ui->roundsSlider->setValue(context->SongControl->getSettings()->getExcRounds());
            ui->roundsValueLabel->setText(QString("%1 rounds").arg(context->SongControl->getSettings()->getExcRounds()));
            ui->roundIncSlider->setValue(context->SongControl->getSettings()->getExcRoundIncrement());
            ui->roundIncValueLabel->setText(QString("%1 notes").arg(context->SongControl->getSettings()->getExcRoundIncrement()));
            ui->offsetSlider->setValue(context->SongControl->getSettings()->getExcStartOffset());
            ui->offsetValueLabel->setText(QString("%1 notes").arg(context->SongControl->getSettings()->getExcStartOffset()));
            ui->loadAsExcersizeCheckbox->setChecked(context->SongControl->getSettings()->getExcMode());
            ui->reverseCheckbox->setChecked(context->SongControl->getSettings()->getExcReturnToStart());
        }

	}
}
