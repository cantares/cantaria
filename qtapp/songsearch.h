#ifndef SONGSEARCH_H
#define SONGSEARCH_H

#include "httphandler.h"
#include "skinmanager.h"
#include "subwindow.h"
#include <QButtonGroup>
#include <QDesktopServices>
#include <QHash>
#include <QLabel>
#include <QList>
#include <QMovie>
#include <QTableWidget>
#include <QTextDocument>
#include <QtXml/QtXml>

/// \cond 0

namespace Ui
{
	class SongSearch;
}

/// \endcond

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
	/*!
	 *  \addtogroup QtGui
	 *  @{
	 */

	//! User Interface
	namespace QtGui
	{
		class SongSearch : public SubWindow
		{
			Q_OBJECT

		public:
			explicit SongSearch(QWidget *parent = nullptr);
			~SongSearch() override;

			void init(CantariaContext *cantariaContext) override;

			void setColumnWidhts(QString columnWidths);
			QString getColumnWidths();

			QDir lastOpenedDir;

		signals:

			/**
			 * @brief A signal sent when a song is picked from the search results
			 *
			 * @param filename The filename of the picked song
			 */
			void searchResultPicked(const QString& filename);

		public slots:
			void skinChanged(const SkinManager *smgr);

		private slots:
			void on_queryField_textChanged(const QString &text);
			void on_addDirButton_clicked();

			void on_resultTable_itemDoubleClicked(QTableWidgetItem *item);

		private:
			Ui::SongSearch *ui;
			const SkinManager *skinMgr;

			int m_timerSearch;

			QFuture<void> m_reindexTask;

			void addDirectory(const std::string &directory);
			void deleteDirectory(const std::string &directory);

			void getSearchDirectories();
			void search();
			void clearSearch();

			void timerEvent(QTimerEvent *event) override;
			void keyPressEvent(QKeyEvent *event) override;
		};

	}// namespace QtGui
}// namespace Cantaria

#endif// SONGSEARCH_H
