#include "audiographwidget.h"

namespace Cantaria {
    namespace QtGui {

        AudioGraphWidget::AudioGraphWidget(QWidget *parent) :
                QWidget(parent),
                cantariaContext(nullptr)
        {
            setAutoFillBackground(false);
        }

        void AudioGraphWidget::setCantariaContext(CantariaContext *context)
        {
            cantariaContext = context;
        }

        void AudioGraphWidget::paintEvent(QPaintEvent *event)
        {
            QPainter painter(this);
            painter.setPen(framePen);
            painter.drawRect(event->rect().adjusted(0,0,-framePen.width(), -framePen.width()));

            QLine audioLine;
            auto* samples = cantariaContext->AudioAnalyzer->getInputSamples();
			if (samples) {
				float sampleCount = cantariaContext->AudioAnalyzer->getSamplesArraySize() - 1;
				float progress = 0.0;
				float prevProgress = 0.0;
				float halfHeight = this->height() / 2;
				painter.setPen(graphPen);

				for (int i = 1; i < width(); i++) {
					progress = i / (float) width();
					prevProgress = (i - 1) / (float) width();
					audioLine.setLine(
							i - 1, halfHeight - samples[(int) (prevProgress * sampleCount)] * halfHeight,
							i, halfHeight - samples[(int) (progress * sampleCount)] * halfHeight);
					painter.drawLine(audioLine);
				}
			}
        }

        QBrush AudioGraphWidget::getGraphColor()
        {
            return graphPen.brush();
        }

        void AudioGraphWidget::setGraphColor(QBrush c)
        {
            graphPen.setBrush(c);
        }

        QBrush AudioGraphWidget::getBorderColor()
        {
            return framePen.brush();
        }

        void AudioGraphWidget::setBorderColor(QBrush c)
        {
            framePen.setBrush(c);
        }

        QBrush AudioGraphWidget::getBackgroundColor()
        {
            return frameBrush;
        }

        void AudioGraphWidget::setBackgroundColor(QBrush c)
        {
            frameBrush = c;
        }
    }
}