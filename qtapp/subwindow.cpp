#include "subwindow.h"

namespace Cantaria
{
	namespace QtGui
	{

		SubWindow::SubWindow(QWidget *parent, Qt::WindowFlags flags) : QFrame(parent, flags),
			context(NULL),
			content(NULL),
			windowBar(NULL),
			sizeGrip(new QSizeGrip(this)),
			allowShow(false)
		{
			setWindowFlags(Qt::SplashScreen);
			sizeGrip->resize(15,15);
		}

		void SubWindow::resizeEvent(QResizeEvent *)
		{
			sizeGrip->move(width() - sizeGrip->width(), height() - sizeGrip->height());

			if(windowBar)
			{
				windowBar->resize(width(), windowBar->height());
				if(content) content->resize(width(), height() - windowBar->height() + 1);
			}
		}

		void SubWindow::setSubWidgets(QWidget *bar, QWidget *winContent)
		{
			windowBar = bar;
			content = winContent;
			sizeGrip->raise();
		}

		void SubWindow::init(CantariaContext* cantariaContext)
		{
			context = cantariaContext;
		}

		bool SubWindow::getAllowShow()
		{
			return allowShow;
		}

		void SubWindow::setAllowShow(bool v)
		{
			allowShow = v;
		}

		void SubWindow::show()
		{
			if(allowShow)
			{
				QFrame::show();
				//Workaround for the problem with layouts and locations of widgets
				resize(size() + QSize(10, 10));
				resize(size() - QSize(10, 10));

				if(content) content->show();
				if(windowBar) windowBar->show();
			}
		}

	}
}
