#include "songsearch.h"
#include "ui_songsearch.h"
#include <QtConcurrent/QtConcurrent>
#include <QtWidgets/QFileDialog>

namespace Cantaria
{
	namespace QtGui
	{

		SongSearch::SongSearch(QWidget *parent) : SubWindow(parent),
												  ui(new Ui::SongSearch),
												  m_timerSearch(0),
												  skinMgr(nullptr)
		{
			ui->setupUi(this);
			SubWindow::setSubWidgets(ui->windowBar, ui->content);

			ui->resultTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
			ui->resultTable->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
		}

		SongSearch::~SongSearch()
		{
			delete ui;
		}

		void SongSearch::init(CantariaContext *cantariaContext)
		{
			SubWindow::init(cantariaContext);

			getSearchDirectories();
		}

		void SongSearch::keyPressEvent(QKeyEvent *event)
		{
			if (event->key() == Qt::Key_Delete)
			{
				//TODO: handle multiple selections
				if (ui->searchDirTable->selectedItems().count() > 0)
				{
					ui->addDirButton->setEnabled(false);
					ui->searchDirTable->setEnabled(false);
					m_reindexTask = QtConcurrent::run(
						this,
						&SongSearch::deleteDirectory,
						ui->searchDirTable->selectedItems().at(0)->text().toStdString());


					int rowCount = ui->searchDirTable->model()->rowCount();
					ui->searchDirTable->clearContents();
					ui->searchDirTable->model()->removeRows(0, rowCount);
				}
			}
		}

		void SongSearch::timerEvent(QTimerEvent *event)
		{
			if (event->timerId() == m_timerSearch)
			{
				search();
				this->killTimer(m_timerSearch);
			}
		}

		void SongSearch::setColumnWidhts(QString columnWidths)
		{
			QStringList widths = columnWidths.split(';');
			for (int i = 0; i < widths.size() && i < ui->resultTable->columnCount() - 1; i++)
			{
				ui->resultTable->setColumnWidth(i, widths[i].toInt());
			}
		}

		QString SongSearch::getColumnWidths()
		{
			QString widths = "";
			for (int i = 0; i < ui->resultTable->columnCount() - 1; i++)
			{
				widths += QString("%1%2").arg(i > 0 ? ";" : "").arg(ui->resultTable->columnWidth(i));
			}
			return widths;
		}

		void SongSearch::skinChanged(const SkinManager *smgr)
		{
			skinMgr = smgr;
			ui->searchIcon->setMovie(new QMovie(skinMgr ? skinMgr->getFileFromSkin("search.png") : ""));
			ui->searchIcon->movie()->start();
		}

		void SongSearch::on_queryField_textChanged(const QString &text)
		{
			if (!text.isEmpty()) {
				m_timerSearch = this->startTimer(500);
			}
		}

		void SongSearch::on_addDirButton_clicked()
		{
			if (!m_reindexTask.isRunning()) {
				ui->addDirButton->setEnabled(false);
				ui->searchDirTable->setEnabled(false);
				int rowCount = ui->searchDirTable->model()->rowCount();
				ui->searchDirTable->clearContents();
				ui->searchDirTable->model()->removeRows(0, rowCount);
				m_reindexTask = QtConcurrent::run(
					this,
					&SongSearch::addDirectory,
					QFileDialog::getExistingDirectory(this, tr("Open Directory"), lastOpenedDir.absolutePath()).toStdString());
			}
		}

		void SongSearch::on_resultTable_itemDoubleClicked(QTableWidgetItem *item)
		{
			if (item) {
				emit searchResultPicked(ui->resultTable->item(item->row(), 0)->text());
			}
		}

		void SongSearch::addDirectory(const std::string &directory)
		{
			QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
			context->SongDatabase->AddDirectory(directory);
			getSearchDirectories();
			QApplication::restoreOverrideCursor();
		}

		void SongSearch::deleteDirectory(const std::string &directory)
		{
			QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
			context->SongDatabase->DeleteDirectory(directory);
			getSearchDirectories();
			QApplication::restoreOverrideCursor();
		}

		void SongSearch::getSearchDirectories()
		{
			QTableWidgetItem *item = nullptr;
			auto dirs = context->SongDatabase->GetDirectories();
			for (auto &dir : dirs) {
				int rowCount = ui->searchDirTable->rowCount();

				item = new QTableWidgetItem(dir.c_str());
				ui->searchDirTable->insertRow(rowCount);
				ui->searchDirTable->setItem(rowCount, 0, item);
			}

			ui->addDirButton->setEnabled(true);
			ui->searchDirTable->setEnabled(true);
		}

		void SongSearch::search()
		{
			ui->searchIcon->setMovie(new QMovie(skinMgr ? skinMgr->getFileFromSkin("wait.gif") : ""));
			ui->searchIcon->movie()->start();
			ui->resultTable->setEnabled(false);
			ui->resultTable->setSortingEnabled(false);

			clearSearch();

			QTableWidgetItem *item = nullptr;
			auto songs = context->SongDatabase->Find(ui->queryField->text().toStdString());
			for (auto &song : songs) {
				int row = ui->resultTable->rowCount();
				QTextDocument htmlEscape;
				ui->resultTable->insertRow(row);

				item = new QTableWidgetItem(song.path.c_str());
				item->setTextAlignment(Qt::AlignCenter);
				ui->resultTable->setItem(row, 0, item);

				item = new QTableWidgetItem(song.filename.c_str());
				item->setTextAlignment(Qt::AlignCenter);
				ui->resultTable->setItem(row, 1, item);

				item = new QTableWidgetItem(song.ext.c_str());
				item->setTextAlignment(Qt::AlignCenter);
				ui->resultTable->setItem(row, 2, item);
			}

			ui->resultTable->setEnabled(true);
			ui->resultTable->setSortingEnabled(true);

			ui->searchIcon->setMovie(new QMovie(skinMgr ? skinMgr->getFileFromSkin("search.png") : ""));
			ui->searchIcon->movie()->start();
		}

		void SongSearch::clearSearch()
		{
			ui->resultTable->clearContents();
			ui->resultTable->model()->removeRows(0, ui->resultTable->model()->rowCount());
		}
	}// namespace QtGui
}// namespace Cantaria
