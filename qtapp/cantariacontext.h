/** @file cantariacontext.h
 * Header file with the CantariaContext class
*/

#ifndef CTRCONTEXT_H
#define CTRCONTEXT_H

#include <core.h>

/// \cond 0

using namespace Cantaria;
using namespace Core;

/// \endcond

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
	/*!
	 *  \addtogroup QtGui
	 *  @{
	 */

	//! User Interface
	namespace QtGui
	{

		/**
		 * @brief Class holding interfaces from the Core library
		 *
		 */
		class CantariaContext
		{
			public:
				CantariaContext();

				ICantariaDevice* SingDevice;	/**< @brief The main device */
				IAudioInput* AudioInput;		/**< @brief The audio input */
				IAudioAnalyzer* AudioAnalyzer;	/**< @brief The audio analyzer */
				ISongControl* SongControl;		/**< @brief The song controller */
				ISongDatabase* SongDatabase;	/**< @brief The song database */
		};
	}  /*! @} End of Doxygen Group Core*/
} /*! @} End of Doxygen Group QtGui*/

#endif // CTRCONTEXT_H
