#ifndef CANTARIA_AUDIOGRAPHWIDGET_H
#define CANTARIA_AUDIOGRAPHWIDGET_H

#include <QtOpenGL/QGLWidget>
#include "cantariacontext.h"
#include <QStyle>
#include <QtGui>

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria {
    /*!
     *  \addtogroup QtGui
     *  @{
     */

    //! User Interface
    namespace QtGui {

        /**
         * @brief Widget for displaying song's notes
         *
         */
        class AudioGraphWidget : public QWidget {
            Q_OBJECT

            Q_PROPERTY(QBrush graphColor READ getGraphColor WRITE setGraphColor)
            Q_PROPERTY(QBrush borderColor READ getBorderColor WRITE setBorderColor)
            Q_PROPERTY(QBrush backgroundColor READ getBackgroundColor WRITE setBackgroundColor)

        protected:
            QPen graphPen;				/**< @brief Stores the graph color */
            QPen framePen;			    /**< @brief Stores the frame color */
            QBrush frameBrush;			    /**< @brief Stores the frame color */

            CantariaContext* cantariaContext;			/**< @brief The context storing interfaces to the Core library*/

        public:
            explicit AudioGraphWidget(QWidget *parent = 0);

            /**
             * @brief Sets the context storing interfaces to the Core library
             *
             * @param context
             */
            void setCantariaContext(CantariaContext* context);

            /**
             * @brief Repaints the widget
             *
             * @param event
             */
            void paintEvent(QPaintEvent *event) override;

            /**
             * @brief Gets the color of the audio graph
             *
             * Set by the stylesheet.
             *
             * @return QBrush The current note color
             */
            QBrush getGraphColor();

            /**
             * @brief Sets the color of the audio graph
             *
             * Set by the stylesheet.
             *
             * @param c The new note color
             */
            void setGraphColor(QBrush c);

            /**
             * @brief Gets the frame color
             *
             * Set by the stylesheet.
             *
             * @return QBrush The current note color
             */
            QBrush getBorderColor();

            /**
             * @brief Sets the frame color
             *
             * Set by the stylesheet.
             *
             * @param c The new note color
             */
            void setBorderColor(QBrush c);

            /**
             * @brief Gets the background color
             *
             * Set by the stylesheet.
             *
             * @return QBrush The current note color
             */
            QBrush getBackgroundColor();

            /**
             * @brief Sets the background color
             *
             * Set by the stylesheet.
             *
             * @param c The new note color
             */
            void setBackgroundColor(QBrush c);

        signals:

        public slots:
        };
    }
}



#endif //CANTARIA_AUDIOGRAPHWIDGET_H
