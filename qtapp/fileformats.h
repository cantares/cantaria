#ifndef AUDIOFORMATS_H
#define AUDIOFORMATS_H

#include <QStringList>

const QString LIST_EXT = "kpl";
const QStringList FILE_FORMATS = (QStringList() << "*.mp3" << "*.ogg" << "*.wav" << "*.aiff" << "*.mid" << "*.kar" << "*.txt" << "*." + LIST_EXT);


#endif // AUDIOFORMATS_H
