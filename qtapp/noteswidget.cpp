#include "noteswidget.h"

namespace Cantaria
{
	namespace QtGui
	{

		NotesWidget::NotesWidget(QWidget *parent) :
            QWidget(parent),
			cantariaContext(NULL),
			pitchMultiplier(0.5),
			lineHeight(0)
		{
			setAutoFillBackground(false);
		}

		void NotesWidget::mousePressEvent(QMouseEvent *event)
		{
			if (event->button() == Qt::LeftButton)
			{
				cantariaContext->SongControl->getSettings()->setSelectionEnd(-1);
				cantariaContext->SongControl->getSettings()->setSelectionStart(xToTime(event->pos().x()));
			}
			event->ignore();
		}

		void NotesWidget::mouseMoveEvent(QMouseEvent *event)
		{
			if (event->buttons() & Qt::LeftButton)
			{
				long xTime = xToTime(event->pos().x());
				if(xTime < cantariaContext->SongControl->getSettings()->getSelectionStart())
					cantariaContext->SongControl->getSettings()->setSelectionStart(xTime);
				else
					cantariaContext->SongControl->getSettings()->setSelectionEnd(xTime);
				event->accept();
			}
		}

		void NotesWidget::mouseReleaseEvent(QMouseEvent *event)
		{
			if (event->button() == Qt::LeftButton &&
					(cantariaContext->SongControl->getSettings()->getSelectionEnd() < 0 ||
					 cantariaContext->SongControl->getSettings()->getSelectionEnd() -
					 cantariaContext->SongControl->getSettings()->getSelectionStart() < 100))
			{
				cantariaContext->SongControl->getSettings()->setSelectionEnd(-1);
				cantariaContext->SongControl->getSettings()->setSelectionStart(-1);
				cantariaContext->SongControl->getSettings()->setCurrentTime(xToTime(event->pos().x()));
				event->accept();
			}
		}

		long NotesWidget::xToTime(float x)
		{
			return (x + timeOffset) / widthMultiplier;
		}

		float NotesWidget::timeToX(long time)
		{
			return time * widthMultiplier - timeOffset;
		}

		void NotesWidget::paintEvent(QPaintEvent *event)
		{
			QPainter painter(this);
			painter.fillRect(event->rect(), painter.background());

			if(!song)
				return;

			track = song->getSelectedTrack();
			notesRange = track ? track->getTopNote() - track->getBottomNote() + TOTAL_MARIGINS + 1 : 16;
			drawNoteLines(event, &painter);

			if(!track)
				return;

			pitchMultiplier = height() / notesRange;
			widthMultiplier = (float)(width()) / settings->getViewTimeRange();
			timeOffset = settings->getViewedTime() * widthMultiplier;

			painter.setRenderHint(QPainter::Antialiasing);
			drawTrack(event, &painter);
			painter.setRenderHint(QPainter::Antialiasing, false);
			drawTimeLine(event, &painter);
			drawRecord(event, &painter);
			drawSelection(event, &painter);

			painter.end();
		}

		void NotesWidget::drawSelection(QPaintEvent *event, QPainter *painter)
		{
			if(cantariaContext->SongControl->getSettings()->getSelectionEnd() > 0)
			{
				QRect selection = event->rect();
				selection.setX(timeToX(cantariaContext->SongControl->getSettings()->getSelectionStart()));
				selection.setRight(timeToX(cantariaContext->SongControl->getSettings()->getSelectionEnd()));

				painter->fillRect(selection, selectionColorVar);
			}
		}

		void NotesWidget::drawNoteLines(QPaintEvent *event, QPainter *painter)
		{
			if(!cantariaContext->SongControl)
				return;

			if (notesRange != lineYs.size())
				lineYs.clear();

			for(int i = 0; i <= notesRange; i++)
			{
				if(lineYs.size() <= i)
				{
					lineYs.push_back(((float)(height() - 1) / notesRange) * i);

					if(lineYs.size() > 1)
						lineHeight = (lineYs[1] - lineYs[0]);
				}

				painter->drawLine(0, lineYs[i], event->rect().width(), lineYs[i]);
			}
		}

		void NotesWidget::drawTrack(QPaintEvent *event, QPainter *painter)
		{
			long endview = cantariaContext->SongControl->getSettings()->getViewedTime() + cantariaContext->SongControl->getSettings()->getViewTimeRange();
			for(int i = track->getNoteIndexForViewTime(); i < track->getNoteCount(); i++)
			{
				const INote* note = track->getNote(i);

				if(note->getStartTime() > endview)
					continue;

				float noteStartPos = timeToX(note->getStartTime() + cantariaContext->SongControl->getSettings()->getOutputLatency());
				float noteEndPos = timeToX(note->getStopTime() + cantariaContext->SongControl->getSettings()->getOutputLatency());
				int currNoteLine = track->getTopNote() + NOTE_MARIGIN - note->getNotePitch();

				if(noteStartPos < width() && noteEndPos > 0 && currNoteLine < lineYs.size())
				{
					QRectF noteRect(noteStartPos, lineYs[currNoteLine],
									noteEndPos-noteStartPos, lineYs[currNoteLine+1] - lineYs[currNoteLine]);

					painter->setBrush(noteColorVar);
					painter->drawRoundedRect(noteRect, 10, noteRect.height());
				}
				else if(noteStartPos > width())
				{
					return;
				}
			}
		}

		void NotesWidget::drawTimeLine(QPaintEvent *event, QPainter *painter)
		{
			float lineLocation = timeToX(settings->getCurrentTime());

			if(lineLocation >= 0 && lineLocation < width())
			{
				painter->drawLine(lineLocation, 0, lineLocation, height());
				int so = cantariaContext->SongControl->getRecord()->getCurrentSampleIndex();
				float currPitch = 0;
				float pitchDiff = 0;

				if(cantariaContext->SongControl->isPlaying() && so > 0 &&
						cantariaContext->SongControl->getRecord()->getSample(so-1)->getState() != IPitchSample::SAMPLE_END)
				{
					currPitch = cantariaContext->SongControl->getRecord()->getSample(so-1)->getPitch();
				}
				else
				{
					currPitch = cantariaContext->AudioAnalyzer->getPitch();

					if(currPitch > 0)
						currPitch += cantariaContext->SongControl->getLastSamplePitchShift();
				}

				if(currPitch > 0)
					currPitchY = (track->getTopNote() + NOTE_MARIGIN - currPitch) * pitchMultiplier - (indicatorVar.height() - lineHeight) / 2;

				if(currPitchY <= 0)
				{
					pitchDiff =  currPitch - track->getTopNote() - NOTE_MARIGIN;
					painter->drawImage(lineLocation, 0, currPitch > 0 ? indicatorHighVar : indicatorHighInactiveVar);
					//painter->drawText(t, QString().setNum(pitchDiff, 'g', 2));
				}
				else if (currPitchY >= height())
				{
					pitchDiff = currPitch - track->getBottomNote() + NOTE_MARIGIN;
					painter->drawImage(lineLocation, height() - indicatorLowVar.height(),
									   currPitch > 0 ? indicatorLowVar : indicatorLowInactiveVar);
				}
				else
				{
					painter->drawImage(lineLocation, currPitchY, currPitch > 0 ? indicatorVar : indicatorInactiveVar);
				}
			}
		}

		void NotesWidget::drawRecord(QPaintEvent *event, QPainter *painter)
		{
			const IPitchRecord* pitchSamples = cantariaContext->SongControl->getRecord();
			int currSample = pitchSamples->getCurrentSampleIndex();
			long viewTime = cantariaContext->SongControl->getSettings()->getViewedTime();
			long endViewTime = viewTime + cantariaContext->SongControl->getSettings()->getViewTimeRange();

			if(currSample <= 0)
				return;

			for(int i = pitchSamples->getStartIndex(viewTime); i < currSample - 1 && pitchSamples->getSample(i)->getTimestamp() < endViewTime; i++)
			{
				if(cantariaContext->SongControl->isRecordSampleVisible(i, NOTE_MARIGIN))
				{
					float currSampleX = timeToX(pitchSamples->getSample(i)->getTimestamp()) - 1;
					float nextSampleX = timeToX(pitchSamples->getSample(i+1)->getTimestamp());
					float sampleY = (track->getTopNote() + NOTE_MARIGIN - pitchSamples->getSample(i)->getPitch()) * pitchMultiplier;
					float accuracy = pitchSamples->getSample(i)->getAccuracy();

					QBrush brush = recBadColorVar;
					if(accuracy > 0.50)
						brush = recPoorColorVar;
					if(accuracy > 0.65)
						brush = recOkColorVar;
					if(accuracy > 0.75)
						brush = recGoodColorVar;
					if(accuracy > 0.85)
						brush = recPerfectColorVar;

					painter->fillRect(QRect(currSampleX, sampleY, nextSampleX - currSampleX, lineYs[1] - lineYs[0]), brush);
				}
			}
		}

		void NotesWidget::setCantariaContext(CantariaContext *context)
		{
			cantariaContext = context;
			settings = cantariaContext->SongControl->getSettings();
			song = cantariaContext->SongControl->getSong();
		}

		void NotesWidget::setNoteColor(QBrush c)
		{
			noteColorVar = c;
		}

		void NotesWidget::setSelectionColor(QBrush c)
		{
			selectionColorVar = c;
		}

		void NotesWidget::setRecPerfectColor(QBrush c)
		{
			recPerfectColorVar = c;
		}

		void NotesWidget::setRecGoodColor(QBrush c)
		{
			recGoodColorVar = c;
		}

		void NotesWidget::setRecOkColor(QBrush c)
		{
			recOkColorVar = c;
		}

		void NotesWidget::setRecPoorColor(QBrush c)
		{
			recPoorColorVar = c;
		}

		void NotesWidget::setRecBadColor(QBrush c)
		{
			recBadColorVar = c;
		}

		void NotesWidget::setIndicator(QImage i)
		{
			indicatorVar = i;
		}

		void NotesWidget::setIndicatorInactive(QImage i)
		{
			indicatorInactiveVar = i;
		}

		void NotesWidget::setIndicatorHigh(QImage i)
		{
			indicatorHighVar = i;
		}

		void NotesWidget::setIndicatorHighInactive(QImage i)
		{
			indicatorHighInactiveVar = i;
		}

		void NotesWidget::setIndicatorLow(QImage i)
		{
			indicatorLowVar = i;
		}

		void NotesWidget::setIndicatorLowInactive(QImage i)
		{
			indicatorLowInactiveVar = i;
		}

		QBrush NotesWidget::getNoteColor()
		{
			return noteColorVar;
		}

		QBrush NotesWidget::getSelectionColor()
		{
			return selectionColorVar;
		}

		QBrush NotesWidget::getRecPerfectColor()
		{
			return recPerfectColorVar;
		}

		QBrush NotesWidget::getRecGoodColor()
		{
			return recGoodColorVar;
		}

		QBrush NotesWidget::getRecOkColor()
		{
			return recOkColorVar;
		}

		QBrush NotesWidget::getRecPoorColor()
		{
			return recPoorColorVar;
		}

		QBrush NotesWidget::getRecBadColor()
		{
			return recBadColorVar;
		}

		QImage NotesWidget::getIndicator()
		{
			return indicatorVar;
		}

		QImage NotesWidget::getIndicatorInactive()
		{
			return indicatorInactiveVar;
		}

		QImage NotesWidget::getIndicatorHigh()
		{
			return indicatorHighVar;
		}

		QImage NotesWidget::getIndicatorHighInactive()
		{
			return indicatorHighInactiveVar;
		}

		QImage NotesWidget::getIndicatorLow()
		{
			return indicatorLowVar;
		}

		QImage NotesWidget::getIndicatorLowInactive()
		{
			return indicatorLowInactiveVar;
		}

	}
}
