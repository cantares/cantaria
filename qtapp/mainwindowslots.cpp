#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "buildopts.h"

namespace Cantaria
{
	namespace QtGui
	{

		void MainWindow::updateSong(bool success, void* userData)
		{
			auto* self = (MainWindow*)userData;
			QString description = "";
			QString title = "";
			const ISong* song = self->cantariaContext->SongControl->getSong();
			if(self->cantariaContext->SongControl->isLoaded())
			{
                title = QString::fromLocal8Bit(song->getProperty(CTR_SONG_PROP_MP3BACKGROUND));
                if(title.isEmpty()) title = QString::fromLocal8Bit(song->getProperty(CTR_SONG_PROP_MIDIFILENAME));

				if(song->getEncoding() == ISong::ANSI)
                    description = QString::fromLocal8Bit(song->getProperty(CTR_SONG_PROP_DESCRIPTION)).replace("\n", "<br />");
				else
                    description = QString::fromUtf8(song->getProperty(CTR_SONG_PROP_DESCRIPTION));
			}

			self->ui->descriptioneLabel->setText(description.length() > 55 ? description.left(52) + "..." : description);
			self->ui->descriptioneLabel->setToolTip(description);
			self->ui->songTitleLabel->setText(title);
			self->ui->viewTimeSlider->setSliderPosition(0);
			self->outputSettings.updateSettings();
			self->settingsWindow.updateSettings();
		}

		void MainWindow::on_playButton_clicked()
		{
#ifdef DEBUG
			lowestFPS = 100;
#endif
			cantariaContext->SongControl->start();
		}

		void MainWindow::on_stopButton_clicked()
		{
			cantariaContext->SongControl->stop();
			cantariaContext->SongControl->rewind();
			settings->setViewedTime(settings->getCurrentTime() < settings->getViewTimeRange() / 2 ? 0 :
																									   settings->getCurrentTime() - settings->getViewTimeRange() / 2);
		}

		void MainWindow::on_pauseButton_clicked()
		{
			cantariaContext->SongControl->stop();
		}

		void MainWindow::on_rewindButton_clicked()
		{
			playlist.loadPrevSong();
		}

		void MainWindow::on_fforwardButton_clicked()
		{
			playlist.loadNextSong();
		}

		void MainWindow::on_viewTimeSlider_sliderMoved(int position)
		{
			settings->setViewedTime((cantariaContext->SongControl->getSong()->getTotalTime() - settings->getViewTimeRange()) * position / 100);
		}


		void MainWindow::on_viewTimeSlider_valueChanged(int value)
		{
			settings->setViewedTime((cantariaContext->SongControl->getSong()->getTotalTime() - settings->getViewTimeRange()) * value / 100);

			if(!detachViewTime)
			{
				settings->setCurrentTime(settings->getViewedTime() + settings->getViewTimeRange() / 2);
			}
		}

		void MainWindow::setSkin(const SkinManager *smgr)
		{
			skinMgr = smgr;
			this->setStyleSheet(smgr->getStyleSheet());
			playlist.setStyleSheet(smgr->getStyleSheet());
			songWindow.setStyleSheet(smgr->getStyleSheet());
			outputSettings.setStyleSheet(smgr->getStyleSheet());
			settingsWindow.setStyleSheet(smgr->getStyleSheet());
			tutorial.setStyleSheet(smgr->getStyleSheet());
			songSearch.setStyleSheet(smgr->getStyleSheet());
		}

		void MainWindow::handleSubwinVisibilisty(SubWindow *window, bool show)
		{
			window->setAllowShow(show);
			if(show) window->show();
			else if (!window->isFullScreen())
				window->hide();
		}

		void MainWindow::on_songCheckbox_clicked(bool checked)
		{
			handleSubwinVisibilisty(&songWindow, checked);
		}

		void MainWindow::on_playlistCheckbox_clicked(bool checked)
		{
			handleSubwinVisibilisty(&playlist, checked);
		}

		void MainWindow::on_outputCheckbox_clicked(bool checked)
		{
			handleSubwinVisibilisty(&outputSettings, checked);
		}

		void MainWindow::on_songSearchCheckbox_clicked(bool checked)
		{
			handleSubwinVisibilisty(&songSearch, checked);
		}

		void MainWindow::on_settingsCheckbox_clicked(bool checked)
		{
			handleSubwinVisibilisty(&settingsWindow, checked);
		}

		void MainWindow::on_ejectButton_clicked()
		{
            playlist.addFile(QFileDialog::getOpenFileName(this, tr("Open File"), playlist.lastOpenedDir.absolutePath(), "Audio Files, UltraStar Txt, Cantaria Playlist (" + FILE_FORMATS.join(" ") + ")"));
		}

		void MainWindow::tutorialPageChange(QString docName, int pageId)
		{
			settingsWindow.setTutorialHighlight(0);
			outputSettings.setTutorialHighlight(false);

			if(docName != "tutorial")
				return;

			switch(pageId)
			{
			case 0:
				break;
			case 1:
				settingsWindow.setAllowShow(true);
				settingsWindow.show();
				songWindow.setAllowShow(true);
				songWindow.show();
				tutorial.raise();
				settingsWindow.setTutorialHighlight(1);
				break;
			case 2:
				settingsWindow.setTutorialHighlight(2);
				break;
			case 3:
				outputSettings.setAllowShow(true);
				outputSettings.show();
				tutorial.raise();
				break;
			case 4:
				playlist.clearPlaylist();
				playlist.addFile("samples/jingle_bells.kar");
				playlist.loadNextSong();
				outputSettings.setTutorialHighlight(true);
				break;
			case 5:
				songSearch.setAllowShow(true);
				songSearch.show();
				tutorial.raise();
			}
		}

		void MainWindow::tutorialClose()
		{
			settingsWindow.setTutorialHighlight(false);
			outputSettings.setTutorialHighlight(false);
			tutorial.close();
		}

		void MainWindow::on_tutorialCheckbox_clicked(bool checked)
		{
			tutorial.setDocumentName("help");
			tutorial.setPage(0);
			if(checked) {
				tutorial.setReplacements({{"%%PROJECT_VERSION%%", PROJECT_VERSION},
					{"%%PROJECT_REVISION%%", PROJECT_REVISION}});
				tutorial.show();
				tutorial.raise();
			} else {
				tutorial.hide();
			}
		}

		void MainWindow::on_checkUpdatesResponse(QNetworkReply *reply)
		{
			if(reply->error()) {
				FILE_LOG(logDEBUG) << "[MainWindow]: Failed fetching updates: " << reply->errorString().toStdString().c_str();
			} else {
				bool updateAvailable = false;
				QString currentVersion = PROJECT_VERSION;
				QJsonObject latestVersion;
				QString downloadList;

				QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
				for(const auto& releaseElement : doc.array()) {
					if(releaseElement.isObject()) {
						const auto& release = releaseElement.toObject();
						if(release["name"].toString().compare(currentVersion) > 0 &&
						   release["name"].toString().compare(latestVersion["name"].toString()) > 0) {
							latestVersion = release;
							updateAvailable = true;
						}
					}
				}

				if(updateAvailable) {
					downloadList = QString("<li><a href=\"%1\">Cantaria v%2</a></li>")
									   .arg(latestVersion["_links"].toObject()["self"].toString())
									   .arg(latestVersion["name"].toString());

					cantariaContext->SongControl->stop();
					tutorial.setDocumentName("updates");
					tutorial.setPage(0);
					tutorial.setReplacements({{"%%DOWNLOADS_LIST%%", downloadList}});
					tutorial.show();
				}
			}

			reply->deleteLater();
		}
	}
}
