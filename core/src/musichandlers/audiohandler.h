/** @file audiohandler.h
 * Header file that switches the between the StreamAudioHandler and MidiHandler classes to be used depending on the values in the implementations.h file
*/

#ifndef STREAMAUDIOHANDLER_H
#define STREAMAUDIOHANDLER_H

#include "../implementations.h"

#if CTR_BASS_STREAM_AUDIO_HANDLER
#include "bassstreamaudiohandler/bassstreamaudiohandler.h"
typedef Cantaria::Core::BassStreamAudioHandler StreamAudioHandler; /**< @brief Use BassStreamAudioHandler for stream audio */
#endif

#if CTR_BASS_MIDI_HANDLER
#include "bassmidihandler/bassmidihandler.h"
typedef Cantaria::Core::BassMidiHandler MidiHandler; /**< @brief Use BassStreamAudioHandler for stream audio */
#endif

#if CTR_JDK_MIDI_HANDLER
#include "jdkmidihandler/jdkmidihandler.h"
typedef Cantaria::Core::JdkMidiHandler MidiHandler;  /**< @brief Use JdkMidiHandler for midi audio */
#endif


#endif // STREAMAUDIOHANDLER_H
