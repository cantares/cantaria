/** @file basscommon.h
 * Common variables and functions for bass music handlers
*/

#include "basscommon.h"

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
    /*!
     *  \addtogroup Core
     *  @{
     */

    //! Core functionalisties: loading files, audio processing, playback control, etc.
    namespace Core
    {
        bool BassCommon::isBassInitialized = false;

        bool BassCommon::initialize()
        {
            if(!BassCommon::isBassInitialized) {
                BassCommon::isBassInitialized = BASS_Init(-1,44100,0, nullptr, nullptr);
            }

            return BassCommon::isBassInitialized;
        }

        void BassCommon::deinitialize()
        {
            if(BassCommon::isBassInitialized) {
                BASS_Free();
                BassCommon::isBassInitialized = false;
            }
        }
    }
}