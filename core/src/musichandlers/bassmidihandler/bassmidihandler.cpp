#include "bassmidihandler.h"

using namespace smf;

namespace Cantaria
{
	namespace Core
	{

		BassMidiHandler::BassMidiHandler(Song* song, ISongSettings* settings) :
			song(song),
			settings(settings),
			mutedTracks(0),
			soloTrack(-1)
		{
			BassCommon::initialize();
		}

		BassMidiHandler::~BassMidiHandler()
		{
            BASS_MIDI_FontFree(soundfont); // free old soundfont
            BassCommon::deinitialize();
		}

		void BassMidiHandler::rewind()
		{
			moveTo(settings->getCurrentTime());
		}

		void BassMidiHandler::moveTo(long time)
		{
			BASS_ChannelSetPosition(stream, BASS_ChannelSeconds2Bytes(stream, ((double)(time))/1000), BASS_POS_BYTE);
		}

		void BassMidiHandler::startPlaying()
		{
			rewind();
			BASS_ChannelPlay(stream, FALSE);
		}

		void BassMidiHandler::stopPlaying()
		{
			BASS_ChannelStop(stream);
		}

		bool BassMidiHandler::isPlaying() const
		{
			return BASS_ChannelIsActive(stream) == BASS_ACTIVE_PLAYING;
		}

		long BassMidiHandler::getCurrTime() const
		{
			return BASS_ChannelBytes2Seconds(stream, BASS_ChannelGetPosition(stream, BASS_POS_BYTE)) * 1000;
		}

		void BassMidiHandler::setSoundfont()
        {
            HSOUNDFONT newfont = BASS_MIDI_FontInit(settings->getSoundfont().c_str(), 0);

            if (newfont) {
                if (soundfont) {
                    BASS_MIDI_FontFree(soundfont);
                    soundfont = 0;
                }

                BASS_MIDI_FONT sf;
                sf.font = newfont;
                sf.preset = -1; // use all presets
                sf.bank = 0; // use default bank(s)
                BASS_MIDI_StreamSetFonts(0, &sf, 1); // set default soundfont
                if(stream) {
                    BASS_MIDI_StreamSetFonts(stream, &sf, 1); // set default soundfont
                }
                soundfont = newfont;
            }
        }

		void BassMidiHandler::setSolo(bool solo, int track)
		{
            soloTrack = solo ? track : -1;
		}

		void BassMidiHandler::setMute(bool mute, int track)
		{
            if (mute) {
                mutedTracks |= (1ull << track);
            } else {
                mutedTracks &= ~(1ull << track);
            }
		}

		void BassMidiHandler::changeVolume()
		{
			BASS_ChannelSetAttribute(stream, BASS_ATTRIB_VOL, (float)settings->getVolume() / 255);
		}

		void BassMidiHandler::changeKey()
		{
		}

		void BassMidiHandler::changeTempo()
		{
            BASS_ChannelSetAttribute(stream, BASS_ATTRIB_MIDI_SPEED, settings->getTempo());
		}

		void BassMidiHandler::updateTrackPlayedTime()
		{
            for(int i = 0; i < song->getTracksCount(); i ++)
            {
                if(i < song->tracks.size() && song->tracks[i]->volume > 0)
                {
                    volumeMtx.lock();
                    song->tracks[i]->volume-=2;
                    volumeMtx.unlock();
                }
            }
		}

		void BassMidiHandler::free()
		{
			if(stream > 0)
			{
				BASS_StreamFree(stream);
				stream = 0;
			}
		}

        BOOL CALLBACK  BassMidiHandler::eventFilter(HSTREAM handle, DWORD track, BASS_MIDI_EVENT *midiEvent, BOOL seeking, void *user)
        {
            BassMidiHandler* self = (BassMidiHandler*)user;
            switch (midiEvent->event) {
                case MIDI_EVENT_NOTE:
                    if (HIBYTE(midiEvent->param) > 0) {
                        if (self->mutedTracks & (1u << track) || (self->soloTrack >= 0 && self->soloTrack != track)) {
                            return false;
                        }

                        self->volumeMtx.lock();
                        self->song->tracks[track]->volume = 254;
                        self->volumeMtx.unlock();

                        midiEvent->param = MAKEWORD(LOBYTE(midiEvent->param) + self->settings->getKeyShift(),
                                                    HIBYTE(midiEvent->param));
                    }
                    break;
                default:
                    break;
            }

            return true;
        }

		bool BassMidiHandler::loadAsSong()
		{
			free();

            midifile.clear();
            if(!midifile.read((song->properties[CTR_SONG_PROP_PATH] + song->properties[CTR_SONG_PROP_MIDIFILENAME]).c_str())) {
                FILE_LOG(logWARNING) << "[BassMidiHandler]: File \"" << (song->properties[CTR_SONG_PROP_PATH] + song->properties[CTR_SONG_PROP_MIDIFILENAME]).c_str() << "\" could not be parsed!";
                return false;
            }

            midifile.doTimeAnalysis();
            midifile.linkNotePairs();

            return parseMidiFile();
		}

        bool BassMidiHandler::loadAsExcersize()
        {
            free();

            midifile.clear();
            if(!midifile.read((song->properties[CTR_SONG_PROP_PATH] + song->properties[CTR_SONG_PROP_MIDIFILENAME]).c_str())) {
                FILE_LOG(logWARNING) << "[BassMidiHandler]: File \"" << (song->properties[CTR_SONG_PROP_PATH] + song->properties[CTR_SONG_PROP_MIDIFILENAME]).c_str() << "\" could not be parsed!";
                return false;
            }

            midifile.doTimeAnalysis();

            double duration = midifile.getFileDurationInSeconds();
            int durationTick = midifile.getFileDurationInTicks();
            int tracks = midifile.getTrackCount();
            MidiEvent* event;
            int rounds = settings->getExcRounds();
            for (int t=0; t < tracks; t++) {
                int trackEvents = midifile[t].size();
                bool returningToStart = false;
                bool roundsReturnToStart = settings->getExcReturnToStart();
                int roundIncrement = settings->getExcRoundIncrement();
                int firstNoteEvent = -1;
                int lastNoteEvent = -1;
                int currentRound = 0;
                int roundMultiplier = 0;
                int keyTop = -1;
                int keyBottom = 255;
                MidiEventList newTrack;

                for (int e=0; e < trackEvents; e++) {
                    event = &midifile[t][e];
                    if (event->isNote()) {
                        firstNoteEvent = e;
                        break;
                    }
                }

                for (int e=trackEvents-1; e >= 0; e--) {
                    event = &midifile[t][e];
                    if (event->isNote()) {
                        lastNoteEvent = e;
                        break;
                    }
                }

                if(firstNoteEvent > 0 && lastNoteEvent > 0) {
                    for (int e = 0; e < trackEvents; e++) {
                        event = &midifile[t][e];
                        MidiEvent eventNew = *event;
                        eventNew.seconds = roundMultiplier * duration + event->seconds;
                        eventNew.tick = roundMultiplier * durationTick + event->tick;
                        eventNew.track = event->track;

                        if (event->isNote()) {
                            if (event->getKeyNumber() > keyTop) {
                                keyTop = event->getKeyNumber();
                            }
                            if (event->getKeyNumber() < keyBottom) {
                                keyBottom = event->getKeyNumber();
                            }

                            eventNew.setKeyNumber(event->getKeyNumber() + currentRound * roundIncrement + settings->getExcStartOffset());
                            eventNew.setVelocity(event->getVelocity());
                        }

                        newTrack.push_back(eventNew);

                        if (e >= lastNoteEvent && currentRound < rounds) {
                            currentRound += (returningToStart ? -1 : 1);

                            if (keyBottom + currentRound * roundIncrement + settings->getExcStartOffset() < 0
                                || keyTop + currentRound * roundIncrement + settings->getExcStartOffset() >= 128) {
                                rounds = currentRound;
                            }

                            if (currentRound >= rounds && roundsReturnToStart) {
                                currentRound -= 2;
                                returningToStart = true;
                            }

                            if (currentRound < rounds && currentRound >= 0) {
                                roundMultiplier++;
                                e = firstNoteEvent - 1; //for loop increments e so we need to go back one more
                            }
                        }
                    }

                    midifile[t].clearSequence();
                    midifile[t] = newTrack;
                }
            }

            midifile.doTimeAnalysis();
            midifile.linkNotePairs();

            bool result = parseMidiFile();

            song->roundDuration = duration * 1000;

			return result;
        }



        bool BassMidiHandler::parseMidiFile()
        {
            std::stringstream midiStream;
            midifile.write(midiStream);
            std::string midistring = midiStream.str();

            stream = BASS_MIDI_StreamCreateFile(true,
                                                midistring.c_str(),
                                                0, midistring.size(), BASS_MIDI_DECAYSEEK, 44100);

            if(!stream) {
                FILE_LOG(logWARNING) << "[BassMidiHandler]: File \"" << (song->properties[CTR_SONG_PROP_PATH] + song->properties[CTR_SONG_PROP_MIDIFILENAME]).c_str() << "\" could not be parsed!";
                return false;
            }

            song->tracks.clear();
            song->songDuration = midifile.getFileDurationInSeconds() * 1000 + settings->getOutputLatency();

            BASS_MIDI_StreamSetFilter(stream, true, BassMidiHandler::eventFilter, this);

            Track* track = nullptr;
            Note* note = nullptr;
            MidiEvent* event;
            int tracks = midifile.getTrackCount();
            int bank = -1;
            int program = -1;
            for (int t=0; t < tracks; t++) {
                bank = -1;
                program = -1;
                track = new Track();
                track->name = "";
                for (int e=0; e < midifile[t].size(); e++) {
                    event = &midifile[t][e];
                    if (event->isNoteOn() && event->getLinkedEvent()) {
                        note = new Note();
                        note->startTime = event->seconds * 1000;
                        note->stopTime = event->getLinkedEvent()->seconds * 1000;
                        note->notePitch = event->getKeyNumber();

                        if (note->notePitch > track->topNote)
                            track->topNote = note->notePitch;
                        if (note->notePitch < track->bottomNote)
                            track->bottomNote = note->notePitch;

                        track->notes.push_back(note);
                    } else if (event->isLyricText() || event->isText() || event->isMarkerText()) {
                        TextData* textData = new TextData();

                        textData->text = event->getMetaContent();
                        textData->time = event->seconds * 1000;
                        textData->type = (ITextData::TextDataType)event->getMetaType();
                        if(textData->time > 0 && textData->text.find('@') != 0)
                        {
                            textData->type =  ITextData::LYRIC_TEXT;
                        }
                        if(textData->type < ITextData::GENERIC_TEXT || textData->type > ITextData::GENERIC_TEXT_F)
                            textData->type = ITextData::GENERIC_TEXT;

                        textData->lineNo = track->textData[textData->type].size() > 0 ? track->textData[textData->type].back()->lineNo : 0;
                        textData->paragraphNo = track->textData[textData->type].size() > 0 ? track->textData[textData->type].back()->paragraphNo : 0;

                        if((textData->text.find('/') != std::string::npos || textData->text.find('\n') != std::string::npos ||
                            textData->text.find('\r') != std::string::npos )
                           && track->textData[ITextData::LYRIC_TEXT].size() > 0)
                            textData->lineNo++;

                        if(textData->text.find('\\') != std::string::npos && track->textData[ITextData::LYRIC_TEXT].size() > 0)
                        {
                            textData->lineNo++;
                            textData->paragraphNo++;
                        }

                        track->textData[textData->type].push_back(textData);
                    } else if (event->isTrackName()) {
                        track->name = event->getMetaContent();
                    } else if (track->name.empty() && event->isInstrumentName()) {
                        track->name = event->getMetaContent();
                    } else if (event->isPatchChange()) {
                        program = event->getP1();
                    } else if (event->isController() && event->getP0() == 176 && event->getP1() == 0) {
                        bank = event->getP2();
                    }
                }

                if (track->name.empty()) {
                    if (program >= 0) {
                        track->name = BASS_MIDI_FontGetPreset(soundfont, program, bank);
                    } else {
                        track->name = "Track " + std::to_string(song->tracks.size() + 1);
                    }
                }
                song->tracks.push_back(track);
            }

            song->clearEmptyTracks();
            return true;
        }

	}
}
