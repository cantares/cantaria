/** @file basscommon.h
 * Common variables and functions for bass music handlers
*/

#ifndef CANTARIA_BASSCOMMON_H
#define CANTARIA_BASSCOMMON_H

#include <bass.h>

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
    /*!
     *  \addtogroup Core
     *  @{
     */

    //! Core functionalisties: loading files, audio processing, playback control, etc.
    namespace Core
    {
        class BassCommon
        {
        private:
            static bool isBassInitialized;

        public:
            static bool initialize();
            static void deinitialize();
        };
    }
}

#endif //CANTARIA_BASSCOMMON_H
