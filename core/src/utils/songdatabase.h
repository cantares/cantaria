#ifndef CANTARIA_SONGDATABASE_H
#define CANTARIA_SONGDATABASE_H

#include "../../include/isongdatabase.h"

#include <sqlite3.h>
#include <string>
#include <vector>


/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
	/*!
     *  \addtogroup Core
     *  @{
     */

	//! Core functionalities: loading files, audio processing, playback control, etc.
	namespace Core
	{

		/**
         * @brief Song database utility
         *
         */
		class SongDatabase : public ISongDatabase
		{
		public:
			explicit SongDatabase(const std::string &filename);
			~SongDatabase() override;

			void AddDirectory(const std::string &directory) override;
			void DeleteDirectory(const std::string &directory) override;
			std::vector<std::string> GetDirectories() override;
			std::vector<Record> Find(const std::string &query) override;

		private:
			sqlite3 *m_db;

			//void createDatabase();
			void indexDirectories();
		};
	}// namespace Core
}// namespace Cantaria


#endif//CANTARIA_SONGDATABASE_H
