#include "songdatabase.h"
#include <ghc/filesystem.hpp>

namespace fs = ghc::filesystem;

namespace Cantaria
{
	namespace Core
	{
		SongDatabase::SongDatabase(const std::string &filename) : m_db(nullptr)
		{
			int rc;
			bool tablesExist = false;
			sqlite3_stmt *stmt;
			std::string sql;

			rc = sqlite3_open_v2(filename.c_str(), &m_db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_FULLMUTEX | SQLITE_OPEN_CREATE, nullptr);

			if (rc) {
				std::string err = std::string(sqlite3_errmsg(m_db));
				throw std::runtime_error("SQLite Error: " + err);
			}

			sql = "SELECT COUNT(*) as c FROM sqlite_master WHERE type='table' AND name IN ('Songs', 'IndexDirectories');";
			sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);

			if (sqlite3_step(stmt) == SQLITE_ROW) {
				tablesExist = (sqlite3_column_int(stmt, 0) == 2);
			}

			rc = sqlite3_step(stmt);
			if (rc != SQLITE_OK && rc != SQLITE_DONE) {
				std::string err = std::string(sqlite3_errmsg(m_db));
				throw std::runtime_error("SQLite Error: " + err);
			}

			if (!tablesExist) {
				sql = "CREATE TABLE Songs ("
					  "path           TEXT PRIMARY KEY NOT NULL,"
					  "filename       TEXT NOT NULL,"
					  "ext  	      TEXT NOT NULL);";

				/* Execute SQL statement */
				rc = sqlite3_exec(m_db, sql.c_str(), nullptr, nullptr, nullptr);

				if (rc != SQLITE_OK) {
					std::string err = std::string(sqlite3_errmsg(m_db));
					throw std::runtime_error("SQLite Error: " + err);
				}

				sql = "CREATE TABLE IndexDirectories ("
					  "path       TEXT PRIMARY KEY NOT NULL);";

				/* Execute SQL statement */
				rc = sqlite3_exec(m_db, sql.c_str(), nullptr, nullptr, nullptr);

				if (rc != SQLITE_OK) {
					std::string err = std::string(sqlite3_errmsg(m_db));
					throw std::runtime_error("SQLite Error: " + err);
				}
			}
		}

		SongDatabase::~SongDatabase()
		{
			if (m_db) {
				sqlite3_close(m_db);
			}
		}

		void SongDatabase::AddDirectory(const std::string &directory)
		{
			int rc = 0;
			sqlite3_stmt *stmt;

			sqlite3_prepare_v2(m_db, "INSERT OR IGNORE INTO IndexDirectories(path) VALUES(?);", -1, &stmt, nullptr);
			sqlite3_bind_text(stmt, 1, directory.c_str(), directory.length(), SQLITE_TRANSIENT);
			rc = sqlite3_step(stmt);

			if (rc != SQLITE_OK && rc != SQLITE_DONE) {
				std::string err = std::string(sqlite3_errmsg(m_db));
				sqlite3_finalize(stmt);

				throw std::runtime_error("SQLite Error: " + err);
			}

			sqlite3_finalize(stmt);
			indexDirectories();
		}

		void SongDatabase::DeleteDirectory(const std::string &directory)
		{
			int rc = 0;
			sqlite3_stmt *stmt;

			sqlite3_prepare(m_db, "DELETE FROM IndexDirectories WHERE path = ?;", -1, &stmt, nullptr);
			sqlite3_bind_text(stmt, 1, directory.c_str(), directory.length(), SQLITE_TRANSIENT);
			rc = sqlite3_step(stmt);

			if (rc != SQLITE_OK && rc != SQLITE_DONE) {
				std::string err = std::string(sqlite3_errmsg(m_db));
				sqlite3_finalize(stmt);

				throw std::runtime_error("SQLite Error: " + err);
			}

			sqlite3_finalize(stmt);
			indexDirectories();
		}

		std::vector<std::string> SongDatabase::GetDirectories()
		{
			std::vector<std::string> result;

			int rc = 0;
			sqlite3_stmt *stmt;
			std::string sql = "SELECT path FROM IndexDirectories;";

			sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);

			while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
				result.emplace_back(reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0)));
			}

			if (rc != SQLITE_OK && rc != SQLITE_DONE) {
				std::string err = std::string(sqlite3_errmsg(m_db));
				sqlite3_finalize(stmt);

				throw std::runtime_error("SQLite Error: " + err);
			}

			return std::move(result);
		}

		std::vector<ISongDatabase::Record> SongDatabase::Find(const std::string &query)
		{
			std::vector<ISongDatabase::Record> result;

			int rc = 0;
			sqlite3_stmt *stmt;
			std::string sql = "SELECT path, filename, ext, '' AS artist, '' AS title "
							  "FROM Songs "
							  "WHERE filename LIKE ('%' || ? || '%') "
							  "ORDER BY filename ASC "
							  "LIMIT 100";

			sqlite3_prepare_v2(m_db, sql.c_str(), -1, &stmt, nullptr);
			sqlite3_bind_text(stmt, 1, query.c_str(), query.length(), SQLITE_TRANSIENT);

			while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
				Cantaria::Core::ISongDatabase::Record rec;
				rec.path = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 0));
				rec.filename = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 1));
				rec.ext = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 2));
				rec.artist = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 3));
				rec.title = reinterpret_cast<const char *>(sqlite3_column_text(stmt, 4));

				result.push_back(rec);
			}

			if (rc != SQLITE_OK && rc != SQLITE_DONE) {
				std::string err = std::string(sqlite3_errmsg(m_db));
				sqlite3_finalize(stmt);

				throw std::runtime_error("SQLite Error: " + err);
			}

			sqlite3_finalize(stmt);
			return std::move(result);
		}

		void SongDatabase::indexDirectories()
		{
			sqlite3_stmt *st;
			int rc = 0, batchSize = 0;
			auto directories = GetDirectories();
			sqlite3_exec(m_db, "DELETE FROM Songs", nullptr, nullptr, nullptr);
			sqlite3_exec(m_db, "BEGIN TRANSACTION", nullptr, nullptr, nullptr);

			sqlite3_prepare(m_db, "INSERT OR IGNORE INTO Songs(path, filename, ext) VALUES(?, ?, ?);", -1, &st, nullptr);

			for (auto &directory : directories)
			{
				for (auto &p : fs::recursive_directory_iterator(directory))
				{
					if (!p.is_directory()) {
						sqlite3_bind_text(st, 1, p.path().c_str(), p.path().string().length(), SQLITE_TRANSIENT);
						sqlite3_bind_text(st, 2, p.path().filename().c_str(), p.path().filename().string().length(), SQLITE_TRANSIENT);
						sqlite3_bind_text(st, 3, p.path().filename().extension().c_str(), p.path().filename().extension().string().length(), SQLITE_TRANSIENT);
						rc = sqlite3_step(st);

						sqlite3_reset(st);
						sqlite3_clear_bindings(st);

						batchSize++;
						if (batchSize > 10000) {
							sqlite3_exec(m_db, "COMMIT TRANSACTION", nullptr, nullptr, nullptr);

							if (rc != SQLITE_OK && rc != SQLITE_DONE) {
								std::string err = std::string(sqlite3_errmsg(m_db));
								sqlite3_finalize(st);

								throw std::runtime_error("SQLite Error: " + err);
							}

							sqlite3_exec(m_db, "BEGIN TRANSACTION", nullptr, nullptr, nullptr);
							batchSize = 0;
						}
					}
				}
			}

			sqlite3_exec(m_db, "COMMIT TRANSACTION", nullptr, nullptr, nullptr);
			sqlite3_finalize(st);
		}
	}// namespace Core
}// namespace Cantaria