#include "songcontrol.h"

namespace Cantaria
{
	namespace Core
	{
		std::string SongControl::getConfigPath()
		{
			return settings.configPath;
		}

		void SongControl::setConfigPath(std::string configPath)
		{
			settings.configPath = configPath;
			settings.exerciseConfigPath = configPath + "exercises/";
		}

		long SongControl::getOutputLatency() const
		{
			return settings.outputLatency;
		}

		long SongControl::getCurrentTime() const
		{
			return settings.currTime;
		}

		long SongControl::getViewedTime() const
		{
			return settings.viewedTime;
		}

		long SongControl::getViewTimeRange() const
		{
			return settings.viewTimeRange;
		}

		char SongControl::getSelectedTrackIndex() const
		{
			return settings.selectedTrackIndex;
		}

		void SongControl::setOutputLatency(long timeShift)
		{
			settings.outputLatency = timeShift;
		}

		void SongControl::setCurrentTime(long currTime)
		{
			if(timeBeyondSelection(currTime))
				currTime = settings.selectionStart;

			settings.currTime = currTime < 0 ? 0 :
											   currTime > song->getTotalTime() ? song->getTotalTime() : currTime;
			pitchRecord->clear(lastRoundSampleIndex);

			if(playing)
				currentHandler->stopPlaying();

			currentHandler->rewind();

			if(playing && !timeBeyondSelection(settings.currTime))
				currentHandler->startPlaying();
		}

		void SongControl::setViewedTime(long viewedTime)
		{
			settings.viewedTime = viewedTime < song->getTotalTime() - settings.viewTimeRange / 2 ? viewedTime :
																							   song->getTotalTime() - settings.viewTimeRange / 2;
		}

		void SongControl::setViewTimeRange(long timePageLength)
		{
			settings.viewTimeRange = timePageLength;
		}

		void SongControl::setSelectedTrackIndex(char index)
		{
			if(index < -1 || index >= song->tracks.size() || song->tracks[index]->notes.size() == 0)
			{
				for(int i = 0; i < song->tracks.size(); i++)
				{
					if(song->tracks[i]->notes.size() > 0)
					{
						settings.selectedTrackIndex = i;
						return;
					}
				}
				settings.selectedTrackIndex = -1;
			}
			else
				settings.selectedTrackIndex = index;
		}

		char SongControl::getKeyShift() const
		{
			return settings.keyShift;
		}

		void SongControl::setKeyShift(char keyShift)
		{
			settings.keyShift = keyShift;
			pitchRecord->clear();
			currentHandler->changeKey();
		}

		float SongControl::getTempo() const
		{
			return settings.tempo;
		}

		void SongControl::setTempo(float tempo)
		{
			if(tempo != settings.tempo)
			{
				pitchRecord->clear();
				float tempoRatio = settings.tempo / tempo;
				settings.currTime *= tempoRatio;
				settings.selectionStart *= tempoRatio;
				settings.selectionEnd *= tempoRatio;
				settings.tempo = tempo;
				currentHandler->changeTempo();
			}
		}

		unsigned char SongControl::getVolume()
		{
			return settings.volume;
		}

		void SongControl::setVolume(unsigned char volume)
		{
			settings.volume = volume;
			currentHandler->changeVolume();
		}

		unsigned char SongControl::getTextGroupBy() const
		{
			return settings.textGroupBy;
		}

		void SongControl::setTextGroupBy(unsigned char groupBy)
		{
			settings.textGroupBy = groupBy;
		}

		unsigned char SongControl::getTextLineCount() const
		{
			return settings.textLineCount;
		}

		void SongControl::setTextLineCount(unsigned char lineCount)
		{
			settings.textLineCount = lineCount;
		}

		long SongControl::getSelectionStart() const
		{
			return settings.selectionStart;
		}

		void SongControl::setSelectionStart(long time)
		{
			settings.selectionStart = time;
		}

		long SongControl::getSelectionEnd() const
		{
			return settings.selectionEnd;
		}

		void SongControl::setSelectionEnd(long time)
		{
			settings.selectionEnd = time;
		}

		bool SongControl::timeBeyondSelection(long time)
		{
			return settings.selectionEnd > 0 &&
					(time < settings.selectionStart || time > settings.selectionEnd);
		}

        std::string SongControl::getSoundfont()
        {
            return settings.soundfont;
        }

        void SongControl::setSoundfont(std::string filename)
        {
            settings.soundfont = filename;
            midiHandler->setSoundfont();
        }

        bool SongControl::getExcMode() const
        {
            return settings.exerciseMode;
        }

        void SongControl::setExcMode(bool exerciseMode)
        {
            settings.exerciseMode = exerciseMode;
			updateExercise();
        }

        int SongControl::getExcRoundIncrement() const
        {
            return settings.excRoundIncrement;
        }

        void SongControl::setExcRoundIncrement(int inc)
        {
            settings.excRoundIncrement = inc;
			updateExercise();
        }

        int SongControl::getExcStartOffset() const
        {
            return settings.excStartOffset;
        }

        void SongControl::setExcStartOffset(int offset)
        {
            settings.excStartOffset = offset;
			updateExercise();
        }

        float SongControl::getExcMinimumAccuracy() const
        {
            return settings.minimumAccuracy;
        }

        void SongControl::setExcMinimumAccuracy(float acc)
        {
            settings.minimumAccuracy = acc;
        }

        int SongControl::getCurrRound() const
        {
            return settings.currRound;
        }

        int SongControl::getExcRounds() const
        {
            return settings.excRounds;
        }

        void SongControl::setExcRounds(int rounds)
        {
            settings.excRounds = rounds;
			updateExercise();
        }

        bool SongControl::getExcReturnToStart() const
        {
            return settings.excReturnToStart;
        }

        void SongControl::setExcReturnToStart(bool returnToStart)
        {
            settings.excReturnToStart = returnToStart;
			updateExercise();
        }
	}
}
