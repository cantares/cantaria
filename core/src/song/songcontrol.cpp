#include <ghc/filesystem.hpp>
#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>
#include "songcontrol.h"

using namespace std::chrono;

namespace Cantaria
{
	namespace Core
	{

		SongControl::SongControl(IAudioAnalyzer* audioAnalyzer) :
			pitchRecord(new PitchRecord()),
			song(new Song()),
			midiHandler(NULL),
			streamHandler(NULL),
			currentHandler(NULL),
			averageAccuracy(0),
			analyzed(false),
			loaded(false),
			playing(false),
			audioAnalyzer(audioAnalyzer)
		{
			midiHandler = new MidiHandler(song, this);
			streamHandler = new StreamAudioHandler(song, this);
			currentHandler = midiHandler;

			settings.selectionStart = -1;
			settings.selectionEnd = -1;
			settings.outputLatency = 350;
			settings.currTime = 0;
			settings.viewedTime = 0;
			settings.selectedTrackIndex = 0;
			settings.keyShift = 0;
			settings.tempo = 1.0;
			settings.viewTimeRange = 5000;
			settings.volume = 255;
			settings.textLineCount = 4;
			settings.textGroupBy = 2;
            settings.exerciseMode = false;
            settings.excRounds = 1;
            settings.excRoundIncrement = 1;
            settings.excStartOffset = 0;
            settings.excReturnToStart = false;

			TextData::settings = this;
			Note::settings = this;
			Track::settings = this;
			Song::settings = this;
		}

		SongControl::~SongControl()
		{
			songLoadedCallbacks.clear();

			delete midiHandler;
			delete streamHandler;
			delete pitchRecord;
			delete song;
		}

		void SongControl::addSongLoadedCallback(CBSONGLOADED *cb, void* userData)
		{
			songLoadedCallbacks.emplace_back(cb, userData);
		}

		bool SongControl::isRecordSampleVisible(int sampleIndex, int marigin) const
		{
			const IPitchSample* currSample = pitchRecord->getSample(sampleIndex);
			const IPitchSample* nextSample = pitchRecord->getSample(sampleIndex+1);
			const ITrack* currTrack = song->getSelectedTrack();

			if(!currSample || !nextSample)
				return false;

			return  currSample->getState() != IPitchSample::SAMPLE_END &&
					nextSample->getState() != IPitchSample::SAMPLE_START &&
					currTrack->isWithinNoteRange(currSample->getPitch(), marigin) &&
					currTrack->isWithinNoteRange(nextSample->getPitch(), marigin) &&
					currSample->getTimestamp() >= settings.viewedTime &&
					nextSample->getTimestamp() <= settings.viewedTime + settings.viewTimeRange;
		}

		bool SongControl::load(const char* filename, bool ignoreConfig)
		{
			stop();

            std::string filenameStr = filename;
			loaded = false;
			rewind();
			song->clear();

			std::string filenameNoExt = filenameStr;
			std::string ext = "";
			std::string path = "";

			size_t sep = filenameNoExt.find_last_of("\\/");
			if (sep != std::string::npos)
			{
				path = filenameNoExt.substr(0, sep + 1);
				filenameNoExt = filenameNoExt.substr(sep + 1, filenameNoExt.size() - sep - 1);
			}

			sep = filenameNoExt.find_last_of(".");
			if (sep != std::string::npos)
			{
				ext = filenameNoExt.substr(sep + 1, filenameNoExt.size() - sep - 1);
				filenameNoExt = filenameNoExt.substr(0, sep+1);
			}

            song->properties[CTR_SONG_PROP_ABSOLUTEPATH] = filenameStr;
            song->properties[CTR_SONG_PROP_PATH] = path;
			currentHandler->free();

			if(ext == "mid" || ext == "kar")
			{
				currentHandler = midiHandler;
                song->encoding = ISong::ANSI;
                song->properties[CTR_SONG_PROP_MIDIFILENAME] = filenameNoExt + ext;
			}
			else if(ext == "txt")
			{
				currentHandler = streamHandler;
                song->encoding = ISong::UTF8;
                song->properties[CTR_SONG_PROP_ULTRASTARTXTFILE] = filenameNoExt + ext;
			}
			else
			{
				currentHandler = streamHandler;
                song->encoding = ISong::UTF8;
                song->properties[CTR_SONG_PROP_MP3BACKGROUND] = filenameNoExt + ext;
                song->properties[CTR_SONG_PROP_ULTRASTARTXTFILE] = filenameNoExt + "txt";
			}

			loaded = currentHandler->loadAsSong();
			if(loaded)
            {
				rewind();
				setViewedTime(0);

				setSelectedTrackIndex(settings.selectedTrackIndex);
				setKeyShift(0);
				setTempo(1.0);
                currentHandler->changeVolume();
			}

			setExcMode(false);
			if(!ignoreConfig && ghc::filesystem::exists(settings.exerciseConfigPath))
			{
				std::ostringstream buf;
				std::ifstream input (settings.exerciseConfigPath + filenameNoExt + ext + ".json");
				if(input.is_open())
				{
					buf << input.rdbuf();

					rapidjson::Document excJson;
					excJson.Parse(buf.str().c_str());

					setExcRounds(excJson["rounds"].GetInt());
					setExcRoundIncrement(excJson["roundIncrement"].GetInt());
					setExcStartOffset(excJson["offset"].GetInt());
					setExcReturnToStart(excJson["returnToStart"].GetBool());

					if (excJson["exerciseMode"].GetBool())
					{
						setExcMode(true);
						loaded &= reloadMusicHandlers();
					}
				}
			}

			for(auto cb : songLoadedCallbacks)
			{
				cb.first(loaded, cb.second);
			}

            return loaded;
		}

		void SongControl::updateExercise()
		{
			if (getExcMode())
			{
				rapidjson::Document excJson;
				excJson.SetObject();
				excJson.AddMember("rounds", getExcRounds(), excJson.GetAllocator());
				excJson.AddMember("roundIncrement", getExcRoundIncrement(), excJson.GetAllocator());
				excJson.AddMember("offset", getExcStartOffset(), excJson.GetAllocator());
				excJson.AddMember("exerciseMode", getExcMode(), excJson.GetAllocator());
				excJson.AddMember("returnToStart", getExcReturnToStart(), excJson.GetAllocator());

				if(!ghc::filesystem::exists(settings.exerciseConfigPath))
					ghc::filesystem::create_directory(settings.exerciseConfigPath);

				std::ofstream exerciseFile;
				exerciseFile.open(settings.exerciseConfigPath + song->getProperty(CTR_SONG_PROP_MIDIFILENAME) + ".json", std::ios::out | std::ios::trunc);

				if (exerciseFile.is_open())
				{
					rapidjson::StringBuffer sb;
					rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(sb);
					excJson.Accept(writer);
					exerciseFile << sb.GetString();
					exerciseFile.close();
				}

                reloadMusicHandlers();
			}
		}

        bool SongControl::reloadMusicHandlers()
        {
		    if (loaded) {
		    	if(settings.exerciseMode)
                	return currentHandler->loadAsExcersize();
		    	else
		    		return currentHandler->loadAsSong();
		    }

		    return false;
        }

		void SongControl::start()
		{
			if(!playing && loaded)
			{
				if (settings.currTime >= song->getTotalTime() - settings.outputLatency ||
						timeBeyondSelection(settings.currTime))
					rewind();

				currentHandler->startPlaying();
				playing = true;

				lastClock = system_clock::now();
			}
		}

		void SongControl::stop()
		{
			currentHandler->stopPlaying();
			playing = false;
		}

		void SongControl::update()
		{
			if(playing)
			{
				if(currentHandler->isPlaying())
					settings.currTime = currentHandler->getCurrTime();
				else
					settings.currTime += duration_cast<milliseconds>(system_clock::now() - lastClock).count();

				currentHandler->updateTrackPlayedTime();

				auto pr = audioAnalyzer->popPitchRecord();
				copyRecord(pr.get(), settings.currTime);

                checkExcercise();

				if (settings.currTime >= song->getTotalTime() + settings.outputLatency ||
						(settings.selectionEnd > 0 && settings.currTime >= settings.selectionEnd))
				{
					stop();
				}

				lastClock = system_clock::now();
			}
		}

		void SongControl::rewind()
		{
			pitchRecord->clear();
			sumAccuracy = 0;
			analyzed = false;

			if(settings.selectionEnd > 0)
			{
				settings.currTime = settings.selectionStart;
				currentHandler->rewind();
				//TODO: Handle lastRoundSampleIndex = 0;
			}
			else
			{
				settings.currTime = 0;
				currentHandler->rewind();
                lastRoundSampleIndex = 0;
			}

            settings.currRound = song->getRoundTime() > 0 ? (settings.currTime + settings.outputLatency) / song->getRoundTime() : 0;
		}

		void SongControl::checkExcercise()
		{
			if(!loaded)
				return;

			if (pitchRecord->getCurrentSampleIndex() - lastRoundSampleIndex > 0)
                averageAccuracy = sumAccuracy / (pitchRecord->getCurrentSampleIndex() - lastRoundSampleIndex);
			else
				averageAccuracy = 0;
            int round = song->getRoundTime() > 0 ? (settings.currTime + settings.outputLatency) / song->getRoundTime() : 0;
            if (round > settings.currRound)
            {
				if(averageAccuracy >= settings.minimumAccuracy) {
					settings.currRound = round;
					lastRoundSampleIndex = pitchRecord->getCurrentSampleIndex();

					FILE_LOG(logDEBUG) << "Total Accuracy: " << averageAccuracy << ", Time Shift: %d" << settings.outputLatency;
				} else {
                    setCurrentTime(settings.currRound * song->getRoundTime());
				}

                averageAccuracy = 0;
                sumAccuracy = 0;
            }
		}

		bool SongControl::isLoaded() const
		{
			return loaded;
		}

		void SongControl::copyRecord(const IPitchRecord* record, long time)
		{
			if(!loaded || song->tracks.size() == 0)
				return;

			int currSample = record->getCurrentSampleIndex()-1;

			if(audioAnalyzer->getAudioInLevel() < audioAnalyzer->getVolumeThreshold())
				pitchRecord->markAsEnd();

			if(currSample < 0)
				return;

			Track* currentTrack = song->tracks[settings.selectedTrackIndex];
			float pitch = record->getSample(currSample)->getPitch();

			const Note* closestNote = NULL;
			float moddedClosest = 0;
			float accuracy = -1;

			for(int i = 0; i < currentTrack->getNoteCount(); i++)
			{
				const Note* currNote = currentTrack->notes[i];

				if(time < currNote->getStopTime() + settings.outputLatency)
				{
					if(settings.currTime < currNote->getStartTime() + settings.outputLatency)
					{
						if(!closestNote)
							pitchRecord->markAsEnd();
						break;
					}

					if(settings.exerciseMode) {
						if(!closestNote || abs(pitch - currNote->getNotePitch()) < abs(pitch - closestNote->getNotePitch())) {
							closestNote = currNote;
						}
					} else {
                        float moddedCurrent = fmod(pitch - currNote->getNotePitch(), 12);
                        if (moddedCurrent > 6)
                            moddedCurrent -= 12;
                        else if (moddedCurrent < -6)
                            moddedCurrent += 12;

                        if(!closestNote ||	abs(moddedCurrent) < abs(moddedClosest) )
                        {
                            closestNote = currNote;
                            moddedClosest = moddedCurrent;
                        }
					}


				}
			}

			if(closestNote)
			{
				if(settings.exerciseMode) {
					accuracy = std::max(1.0f - std::abs(pitch - closestNote->getNotePitch()), 0.0f);
                    lastSamplePitchShift = 0;
				} else {
					pitch = closestNote->getNotePitch() + moddedClosest;
                    accuracy = std::max(1.0f - std::abs(moddedClosest), 0.0f);
                    lastSamplePitchShift = pitch - record->getSample(currSample)->getPitch();
				}

				sumAccuracy += accuracy;
				pitchRecord->addSample(pitch, time, accuracy);
			}
		}

		int SongControl::getLastSamplePitchShift() const
		{
			return lastSamplePitchShift;
		}

        float SongControl::getAccuracy() const
        {
            return averageAccuracy;
        }

		const IPitchRecord* SongControl::getRecord() const
		{
			return pitchRecord;
		}

		const ISong* SongControl::getSong() const
		{
			return song;
		}

		ISongSettings* SongControl::getSettings()
		{
			return this;
		}

		const ISongSettings* SongControl::getSettings() const
		{
			return this;
		}

		bool SongControl::isPlaying() const
		{
			return playing;
		}

		void SongControl::setMute(bool mute, int track)
		{
			currentHandler->setMute(mute, track);
		}

		void SongControl::setSolo(bool solo, int track)
		{
			currentHandler->setSolo(solo, track);
		}
	}
}
