#include "device.h"
#include <ghc/filesystem.hpp>

namespace fs = ghc::filesystem;

namespace Cantaria
{
	namespace Core
	{

		CantariaDevice::CantariaDevice(const std::string &configDirectory) : m_audioAnalyzer(new AudioAnalyzer()),
																			 m_audioInput(new AudioInput(m_audioAnalyzer)),
																			 m_songControl(new SongControl(m_audioAnalyzer)),
																			 m_songDatabase(nullptr)
		{
			Output2FILE::Stream() = fopen("cantaria_core.log", "w");
			if(!fs::exists(configDirectory)) {
				fs::create_directory(configDirectory);
			}

			m_songDatabase = new SongDatabase(configDirectory + "cantaria.sqlite");
			m_songControl->getSettings()->setConfigPath(configDirectory);
		}

		CantariaDevice::~CantariaDevice()
		{
			delete m_audioInput;
			delete m_audioAnalyzer;
			delete m_songControl;
			delete m_songDatabase;
		}

		IAudioAnalyzer *CantariaDevice::getAudioAnalyzer()
		{
			return m_audioAnalyzer;
		}

		IAudioInput *CantariaDevice::getAudioInput()
		{
			return m_audioInput;
		}
		ISongControl *CantariaDevice::getSongControl()
		{
			return m_songControl;
		}

		const IAudioAnalyzer *CantariaDevice::getAudioAnalyzer() const
		{
			return m_audioAnalyzer;
		}

		const IAudioInput *CantariaDevice::getAudioInput() const
		{
			return m_audioInput;
		}

		const ISongControl *CantariaDevice::getSongControl() const
		{
			return m_songControl;
		}

		ISongDatabase *CantariaDevice::getSongDatabase() const
		{
			return m_songDatabase;
		}

		void CantariaDevice::release()
		{
			delete this;
		}
	}// namespace Core
}// namespace Cantaria
