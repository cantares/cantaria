/** @file device.h
 * Header file with the CantariaDevice class
*/

#pragma once

#include "log.h"
#include "../include/idevice.h"
#include "audio/audioanalyzer/audioanalyzer.h"
#include "audio/audioio/audioinput.h"
#include "song/songcontrol.h"
#include "utils/songdatabase.h"
#include <fstream>

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
	/*!
	 *  \addtogroup Core
	 *  @{
	 */

	//! Core functionalisties: loading files, audio processing, playback control, etc.
	namespace Core
	{
		/**
		 * @brief Class implementing the ICantariaDevice
		 *
		 */
		class CantariaDevice : public ICantariaDevice
		{
			private:
				IAudioAnalyzer*m_audioAnalyzer; /**< @brief Audio analyzer interface */
				IAudioInput*m_audioInput; /**< @brief Audio input interface */
				ISongControl*m_songControl; /**< @brief Song control interface */
				ISongDatabase* m_songDatabase; /**< @brief Song database interface */

			public:
				explicit CantariaDevice(const std::string& configDirectory);
				~CantariaDevice() override;

				/**
				 * @brief Returns the audio analyzer interface
				 *
				 * @return IAudioAnalyzer The audio analyzer interface
				 */
				virtual IAudioAnalyzer* getAudioAnalyzer();

				/**
				 * @brief Returns the audio input interface
				 *
				 * @return IAudioInput The audio input interface
				 */
				virtual IAudioInput* getAudioInput();

				/**
				 * @brief Returns the song control interface
				 *
				 * @return ISongControl The song control interface
				 */
				virtual ISongControl* getSongControl();

				/**
				 * @brief Releases the device. Must be called before closing the program
				 *
				 */
				virtual void release();

				/**
				 * @brief Returns a readonly audio analyzer interface
				 *
				 * @return IAudioAnalyzer The audio analyzer interface
				 */
				virtual const IAudioAnalyzer* getAudioAnalyzer() const;

				/**
				 * @brief Returns a readonly audio input interface
				 *
				 * @return IAudioInput The audio input interface
				 */
				virtual const IAudioInput* getAudioInput() const;

				/**
				 * @brief Returns a readonly song control interface
				 *
				 * @return ISongControl The song control interface
				 */
				virtual const ISongControl* getSongControl() const;

				ISongDatabase* getSongDatabase() const override;
		};
	}  /*! @} End of Doxygen Group Core*/
} /*! @} End of Doxygen Group Cantaria*/
