/** @file aubioanalyzer.h
 * Header file with the AubioAnalyzer class
*/

#ifndef _AUBIO_SOUND_PROCESSOR
#define _AUBIO_SOUND_PROCESSOR

#define PITCH_SAMPLES_COUNT 5

#include "../../../../include/iaudioanalyzer.h"
#include "../../../../include/isongsettings.h"
#include "../../pitchrecord/pitchrecord.h"
#include "aubiofuncs.h"
#include "log.h"

#include <math.h>
#include <float.h>
#include <mutex>

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
	/*!
	 *  \addtogroup Core
	 *  @{
	 */

	//! Core functionalisties: loading files, audio processing, playback control, etc.
	namespace Core
	{

		/**
		 * @brief IAudioAnalyzer implementation using the Aubio library
		 *
		 */
		class AubioAnalyzer : public IAudioAnalyzer
		{
			private:
				// ------------------------------------------------------------------
				// internal aubio variables

				unsigned int vector_size;					/**< @brief Samples vector size */
				unsigned int hop_s;							/**< @brief Hop size */
				unsigned int samplerate;					/**< @brief Samplerate */
				unsigned int channels;						/**< @brief Number of channels */

				unsigned int input_vec_idx;
				fvec_t * input_vec[2]; /**< @brief Vector with the audio samples to be analyzed  */
                aubio_pitch_t* pitch_output ; /**< @brief The pitch detector */

				// ------------------------------------------------------------------

				float samplesBuffer[BUFFER_SIZE]; /**< @brief The buffer with audio samples */

                char_t const * detectionMode; /**< @brief Aubio pitch detection mode */
                char_t const * detectionType; /**< @brief Aubio pitch detection type */
				float volumeThreshold; /**< @brief The volume threshold below which the samples won't be further analyzed */
				unsigned int newVectorSize; /**< @brief The new size of the samples vector that was requested */
				bool typeChanged; /**< @brief Pitch detection type was changed */

                std::shared_ptr<PitchRecord> pitchRecord; /**< @brief The record of the processed samples */
				fvec_t* pitch; /**< @brief The current pitch */
				float audiInLevel; /**< @brief The current amplitude */

                std::mutex pitchMtx;

			public:
				/**
				 * @brief Default constructor for the Aubio Analyzer
				 *
				 */
				AubioAnalyzer();
				/**
				 * @brief Destructor for the Aubio Analyzer
				 *
				 */
				~AubioAnalyzer() override;

				/**
				 * @brief Sets up the pitch detector and samples vector
				 *
				 */
				void setup();

				/**
				 * @brief Processes the captured audio samples, and stores the information in the PitchRecord
				 *
				 * @param buffer The array of audio samples
				 * @param bufferSize The size of the array
				 */
				void processAudio(AUDIO_DATA_TYPE* buffer, int bufferSize) override;

				/**
				 * @brief Gets the pitch of the most recently captured audio samples
				 *
				 * @return float The pitch of the captured audio samples
				 */
				float getPitch() const override;

				/**
				 * @brief Gets the amplitude of the most recently captured audio samples
				 *
				 * @return float The amplitude of the captured audio samples
				 */
				float getAudioInLevel() const override;

				/**
				 * @brief Gets the volume threshold, below which the sample will not be further analyzed
				 *
				 * @return float The volume threshold
				 */
				float getVolumeThreshold() const override;

				/**
				 * @brief Sets the volume threshold, below which the sample will not be further analyzed
				 *
				 * @param threshold The volume threshold
				 */
				void setVolumeThreshold(float threshold) override;

				/**
				 * @brief Gets the size of the array of samples to analyze
				 *
				 * @return int The size of the array
				 */
				int getSamplesArraySize() const override;

				/**
				 * @brief Sets the size of the array of samples to analyze
				 *
				 * @param int The size of the array
				 */
				void setSamplesArraySize(int hopSize) override;

                /**
                 * @brief Fetches the current audio input data. Use getSamplesArraySize for current size of array
                 *
                 * @return pointer to the first audio sample
                 */
                const AUDIO_DATA_TYPE* getInputSamples() const override;

				/**
				 * @brief Gets a copy ofthe record of processed samples
				 *
				 * @return IPitchRecord The record of processed samples
				 */
				std::shared_ptr<IPitchRecord> popPitchRecord() override;
		};

	}  /*! @} End of Doxygen Group Core*/
} /*! @} End of Doxygen Group Cantaria*/

#endif	

