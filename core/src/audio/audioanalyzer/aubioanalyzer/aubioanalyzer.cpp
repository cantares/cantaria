#include "aubioanalyzer.h"

namespace Cantaria
{
	namespace Core
	{

		AubioAnalyzer::AubioAnalyzer() :
			//aubio stuff
			vector_size(1024),				/* window size */
			hop_s(vector_size),							/* hop size */
			samplerate(AUDIO_SAMPLERATE),		/* samplerate */
			channels(1),						/* number of channel */
			pitch_output(nullptr),

            detectionMode("cent"),				//change to "midi" to get frequency as midi notes
			detectionType("yinfft"),
            volumeThreshold(0.6),
            newVectorSize(vector_size),
            typeChanged(false),

            pitchRecord(new PitchRecord()),
            pitch(0),
            audiInLevel(0),
            input_vec_idx(0)
		{

            input_vec[0] = nullptr;
            input_vec[1] = nullptr;
		}

		//--------------------------------------------------------------

		void AubioAnalyzer::setup()
		{
            if(vector_size != newVectorSize || typeChanged)
            {
                vector_size = newVectorSize;
                hop_s = vector_size;

                if(input_vec[0]) {
                    del_fvec(input_vec[0]);
                    del_fvec(input_vec[1]);
                }

                if(pitch)
                    del_fvec(pitch);

                if(pitch_output)
                    del_aubio_pitch(pitch_output);

                input_vec[0] = nullptr;
                input_vec[1] = nullptr;
                pitch = nullptr;
                pitch_output = nullptr;
                typeChanged = false;
            }

            //setup aubio buffer and pitch detection
            if(input_vec[0] == nullptr) {
                input_vec[0] = new_fvec(vector_size);
                input_vec[1] = new_fvec(vector_size);
                fvec_zeros(input_vec[0]);
                fvec_zeros(input_vec[1]);
                input_vec_idx = 0;
            }
            if(pitch == nullptr)
                pitch  = new_fvec (1);

            if(pitch_output == nullptr) {
                pitch_output = new_aubio_pitch(detectionType, vector_size, hop_s, samplerate);
                aubio_pitch_set_unit (pitch_output, detectionMode);
                //aubio_pitch_set_tolerance(pitch_output, detectionMode);
            }
		}

		//--------------------------------------------------------------
		AubioAnalyzer::~AubioAnalyzer()
		{
            FILE_LOG(logDEBUG) << "[AubioAnalyzer]: Cleaning up aibio";
            if(pitch_output)
            {
                del_aubio_pitch(pitch_output);
                FILE_LOG(logDEBUG) << "[AubioAnalyzer]: Delete pitch detection success";
            }
            if(input_vec[0])
            {
                del_fvec(input_vec[0]);
                del_fvec(input_vec[1]);
                FILE_LOG(logDEBUG) << "[AubioAnalyzer]: Delete fvec success";
            }

			if(pitchRecord)
			{
				//delete pitchRecord;
				FILE_LOG(logDEBUG) << "[AubioAnalyzer]: Delete pitch record success";
			}

			FILE_LOG(logDEBUG) << "[AubioAnalyzer]: Analyzer release success";
		}

		//--------------------------------------------------------------
		void AubioAnalyzer::processAudio(AUDIO_DATA_TYPE* buffer, int bufferSize)
		{
			setup();

			if(bufferSize < vector_size) {
                memcpy(input_vec[input_vec_idx]->data + vector_size - bufferSize, buffer, bufferSize*4);
                memcpy(input_vec[input_vec_idx]->data, input_vec[(input_vec_idx+1)%2]->data + bufferSize - 1, (vector_size - bufferSize)*4);
			} else {
                memcpy(input_vec[input_vec_idx]->data, buffer, vector_size*4);
			}

			audiInLevel = aubio_level_lin(input_vec[input_vec_idx]);

			//don't update the pitch if the sound is very quiet
			if(audiInLevel > volumeThreshold && input_vec[input_vec_idx] != NULL && pitch_output != NULL)
			{
				//finally get the pitch of the sound
				aubio_pitch_do (pitch_output, input_vec[input_vec_idx], pitch); // - octaveShift * 12;
				float pitch_result = fvec_get_sample(pitch, 0);

                pitchMtx.lock();
				if(pitch_result > 0 && pitch_result < FLT_MAX && pitch_result > FLT_MIN)
				{
					pitchRecord->addSample(pitch_result);
				}
				else
				{
					pitchRecord->markAsEnd();
				}
                pitchMtx.unlock();
			}
			else
			{
				fvec_set_sample(pitch, 0, 0);
                pitchMtx.lock();
				pitchRecord->markAsEnd();
                pitchMtx.unlock();
			}

            input_vec_idx++;
            input_vec_idx %= 2;
		}

		float AubioAnalyzer::getPitch() const
		{
			return pitch ? fvec_get_sample(pitch, 0) : 0;
		}

        float AubioAnalyzer::getVolumeThreshold() const
        {
            return std::pow(volumeThreshold, 0.25);
        }

        void AubioAnalyzer::setVolumeThreshold(float threshold)
        {
            volumeThreshold = std::pow(threshold, 4);
        }

		float AubioAnalyzer::getAudioInLevel() const
		{
			return std::pow(audiInLevel, 0.25);
		}

		int AubioAnalyzer::getSamplesArraySize() const
		{
			return newVectorSize;
		}

		void AubioAnalyzer::setSamplesArraySize(int size)
		{
			newVectorSize = size;
		}

        const AUDIO_DATA_TYPE* AubioAnalyzer::getInputSamples() const
        {
			if (!input_vec || !input_vec[input_vec_idx])
                return nullptr;

            return input_vec[input_vec_idx]->data;
		}

        std::shared_ptr<IPitchRecord> AubioAnalyzer::popPitchRecord()
		{
            auto pitchRecordCopy = std::make_shared<PitchRecord>();
            pitchMtx.lock();
            pitchRecordCopy.swap(pitchRecord);
            pitchMtx.unlock();
			return std::move(pitchRecordCopy);
		}

	}
}
