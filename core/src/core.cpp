#include "../include/core.h"
#include "device.h"

namespace Cantaria
{
	namespace Core
	{

		ICantariaDevice* createCantariaDevice(const std::string& configDirectory)
		{
			return new CantariaDevice(configDirectory);
		}
	}
}
