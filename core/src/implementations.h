/** @file implementations.h
 * Header file that allows to quickly configure which classes are going to be used to implement given interfaces
*/

#ifndef IMPLEMENTATIONS_H
#define IMPLEMENTATIONS_H

/** @def CTR_RT_AUDIO_INPUT
 * @brief Use RtAudio Implementation of IAudioInput */
/** @def CTR_BASS_AUDIO_INPUT
 * @brief Use BASS Implementation of IAudioInput */

/** @def CTR_AUBIO_ANALYZER
 * @brief Use Aubio Implementation of IAudioAnalyzer */

/** @def CTR_BASS_STREAM_AUDIO_HANDLER
 * @brief Use BASS to handle audio streams */

/** @def CTR_JDK_MIDI_HANDLER
 * @brief Use JdksMidi to handle midi music */

#define CTR_RT_AUDIO_INPUT				0
#define CTR_BASS_AUDIO_INPUT			1

#define CTR_AUBIO_ANALYZER				1

#define CTR_BASS_STREAM_AUDIO_HANDLER	1

#define CTR_JDK_MIDI_HANDLER			0
#define CTR_BASS_MIDI_HANDLER		    1

#endif // IMPLEMENTATIONS_H
