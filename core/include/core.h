/** @file core.h
 * Header file with the createCantariaDevice function
*/

#ifndef CTR_CORE_H__
#define CTR_CORE_H__

#if CTRCORE_LIBRARY || BUILDING_DLL
# define DLLIMPORT __declspec (dllexport)
#else /* Not BUILDING_DLL */

#endif /* Not BUILDING_DLL */

#include "idevice.h"

namespace Cantaria
{
	namespace Core
	{
		/**
		 * @brief Creates the main Cantaria device
		 *
		 * @return ISingDevice
		 */
		ICantariaDevice* createCantariaDevice(const std::string& configDirectory);
	}
}


#endif

