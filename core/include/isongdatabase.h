/** @file isongdatabase.h
 * Header file with the ISongDatabase interface
*/

#pragma once

#include <string>
#include <vector>

/*!
 *  \addtogroup Cantaria
 *  @{
 */

//! Main Cantaria namespace
namespace Cantaria
{
	/*!
	 *  \addtogroup Core
	 *  @{
	 */

	//! Core functionalities: loading files, audio processing, playback control, etc.
	namespace Core
	{
		/**
		 * @brief Interface for accessing the properties of a IPitchRecord sample
		 *
		 */
		class ISongDatabase
		{
			public:

			struct Record
			{
				std::string path;
				std::string filename;
				std::string ext;
				std::string artist;
				std::string title;
			};


			virtual ~ISongDatabase() {}

			/**
			 * @brief Add directory to the song database
			 *
			 */
			virtual void AddDirectory(const std::string &directory) = 0;

			/**
			 * @brief Remove directory from the song database
			 *
			 */
			virtual void DeleteDirectory(const std::string &directory) = 0;

			/**
			 * @brief Get the list of directories added to the song database
			 *
			 * @return vector with the list of paths
			 */
			virtual std::vector<std::string> GetDirectories() = 0;

			/**
			 * @brief Find songs in the database
			 *
			 */
			virtual std::vector<Record> Find(const std::string &query) = 0;
		};
	}  /*! @} End of Doxygen Group Core*/
} /*! @} End of Doxygen Group Cantaria*/
